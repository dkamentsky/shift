﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shift
{
    public class ChangeGravityVolume : MonoBehaviour
    {
        public ShiftAxis shiftToAxis; [Tooltip("axis to shift gravity to in world space")]

        protected GravityShifter gravityShifter;

        public void Update()
        {
            if(gravityShifter == null)
                gravityShifter = AManager.instance.gravityShifter; 
        }

        protected void OnTriggerEnter(Collider other)
        {
            if (other.GetComponent<CharacterMotor>())
                gravityShifter.ShiftToWorldGravity(shiftToAxis);
        }
    }
}
