﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Shift
{
    [CustomEditor(typeof(LOD_DisableEnable))]
    public class LOD_DisableEnableScript : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            LOD_DisableEnable myscript = (LOD_DisableEnable)target;
            if (GUILayout.Button("SetLODs"))
                myscript.ToggleLODs();
        }
    }
}