﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Shift
{ 
    [CustomEditor(typeof(CollapseRebuildEvent))]    
    public class CollapseRebuildEventScript : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            CollapseRebuildEvent myscript = (CollapseRebuildEvent)target;
            if (GUILayout.Button("Populate Lists"))
                myscript.PopulateList();
        }
    }
}