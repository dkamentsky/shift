﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Shift
{ 
    [CustomEditor(typeof(TileManager))]
    public class TileManagerScript : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            TileManager myscript = (TileManager)target;
            if (GUILayout.Button("Sort List"))
                myscript.SortTiles();
        }
    }
}