﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Shift
{
    [CustomEditor(typeof(SceneProgressManager))]
    public class SceneProgressManagerScript : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            SceneProgressManager myscript = (SceneProgressManager)target;

            EditorGUILayout.BeginHorizontal("box");
            EditorGUILayout.LabelField((myscript.triggerProgress + 1).ToString());
            EditorGUILayout.EndHorizontal();
            if(myscript.triggerList.Count > 1)
            {
                foreach(ShiftTrigger trigger in myscript.triggerList)
                {
                    EditorGUILayout.BeginHorizontal("Box");
                    EditorGUILayout.LabelField(trigger.name);
                    int valueToSwap = myscript.triggerList.IndexOf(trigger) + 1;
                    myscript.SetTriggerNumber(EditorGUILayout.IntField(valueToSwap) - 1, trigger);
                    EditorGUILayout.BeginVertical();
                    if(myscript.triggerList.IndexOf(trigger)>0)
                    {
                        if (GUILayout.Button("^"))
                            myscript.MoveTriggerUp(trigger);
                    }
                    if (myscript.triggerList.IndexOf(trigger) < myscript.triggerList.Count)
                    {
                        if (GUILayout.Button("v"))
                            myscript.MoveTriggerDown(trigger);
                    }
                    EditorGUILayout.EndVertical();
                    EditorGUILayout.EndHorizontal();
                }
            }

            if (GUILayout.Button("Get All Triggers"))
                myscript.GetAllTriggers();
            if (GUILayout.Button("Save List Order"))
                myscript.SaveListOrder();
            if (GUILayout.Button("Load List Order"))
                myscript.LoadListOrder();
        }
    }
}