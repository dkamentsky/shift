﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Shift
{
    [CustomEditor(typeof(DefaultLevelState))]
    public class DefaultLevelStateScript : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            DefaultLevelState myscript = (DefaultLevelState)target;
            if (GUILayout.Button("SetAllActive"))
                myscript.SetAllActive();
            if (GUILayout.Button("SetAllInActive"))
                myscript.SetAllInActive();
            if (GUILayout.Button("SetDefaultState"))
                myscript.SetDefaultState();
        }
    }
}