﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Shift
{
    [Serializable]
    public class Button
    {
        public KeyCode button;
        public bool bIsActive = true;
        [HideInInspector]
        public bool bIsPressed = false;

        public Button (KeyCode m_Button,bool m_IsActive)
        {
            button = m_Button;
            bIsActive = m_IsActive;
        }

        public void CheckPress()
        {
            if (Input.GetKeyDown(button) && bIsActive)
                bIsPressed = true;
            else
                bIsPressed = false;
        }
    }

    public class PlayerController : MonoBehaviour
    {
        public string horizontalAxisName = "Horizontal";
        public string verticalAxisName = "Vertical";
        public Button jumpButton = new Button(KeyCode.Space, true);
        public Button shiftKey = new Button(KeyCode.LeftShift, true);
        public Button reverseShiftKey = new Button(KeyCode.C, true);
        public Button leftShiftKey = new Button(KeyCode.Q, true);
        public Button rightShiftKey = new Button(KeyCode.E, true);

        protected List<Button> buttonList = new List<Button>();

        [HideInInspector]
        public Vector2 moveInput;

        public bool bDebugThisClass = false;

        protected void Start()
        {
            buttonList.Add(jumpButton);
            buttonList.Add(shiftKey);
            buttonList.Add(reverseShiftKey);
            buttonList.Add(leftShiftKey);
            buttonList.Add(rightShiftKey);
        }

        protected void Update()
        {
            foreach (Button button in buttonList)
                button.CheckPress();
            moveInput = GetInput();
        }

        public Vector2 GetInput()
        {
            Vector2 input = new Vector2
            {
                x = Input.GetAxis(horizontalAxisName),
                y = Input.GetAxis(verticalAxisName)
            };
            return input;
        }
    }
}