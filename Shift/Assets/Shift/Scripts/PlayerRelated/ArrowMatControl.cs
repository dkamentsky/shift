﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shift
{
    public enum GravArrow
    {
        forwardArrow,
        backArrow,
        rightArrow,
        leftArrow,
    }
    public class ArrowMatControl : MonoBehaviour
    {
        public Material forwardArrowMat;
        public Material backArrowMat;
        public Material rightArrowMat;
        public Material leftArrowMat;
        public Color emmissiveColor;
        public string emmisivePropertyName = "_EmissionColor";
        protected int emmissiveID;
        protected Color deffaultEmmisiveColor;

        protected void OnApplicationQuit()
        {
            SetAllToDefault();
        }

        protected void Start()
        {
            emmissiveID = Shader.PropertyToID(emmisivePropertyName);
            deffaultEmmisiveColor = forwardArrowMat.GetColor(emmissiveID);
        }

        public void SetAllToDefault()
        {
            SetArrowMaterial(forwardArrowMat, false);
            SetArrowMaterial(backArrowMat, false);
            SetArrowMaterial(leftArrowMat, false);
            SetArrowMaterial(rightArrowMat, false);
        }

        public void ArrowActivate(GravArrow arrow, bool bGravState)
        {
            switch(arrow)
            {
                case GravArrow.forwardArrow:
                    SetArrowMaterial(forwardArrowMat, bGravState);
                    return;
                case GravArrow.backArrow:
                    SetArrowMaterial(backArrowMat, bGravState);
                    return;
                case GravArrow.leftArrow:
                    SetArrowMaterial(leftArrowMat, bGravState);
                    return;
                case GravArrow.rightArrow:
                    SetArrowMaterial(rightArrowMat, bGravState);
                    return;
                default:
                    Debug.Log("Invalid Input");
                    return;
            }
        }

        protected void SetArrowMaterial(Material mat, bool bGravState)
        {
            if (bGravState)
                mat.SetColor(emmissiveID, emmissiveColor);
            else
                mat.SetColor(emmissiveID, deffaultEmmisiveColor);
        }
    }
}