﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shift
{
    public class PlayerAnimation : MonoBehaviour
    {
        public Animator anim;
        public Rigidbody playerRB;
        public float velocityDamper = 10;
        public List<GameObject> unhideForGrav = new List<GameObject>();
        public float shiftAnimTime = 1;

        protected GravityShifter gravityShifter;
        protected float walkSpeed = 0;
        protected int walkHash = Animator.StringToHash("Walk");
        protected int hasGravHash = Animator.StringToHash("HasGrav");
        protected int backForwarLeftRightHash = Animator.StringToHash("BackForwardLeftRight");
        protected bool bShowGrav = false;
        protected bool bPlayShiftAnimation = false;
        protected float internalShiftTime = 0;
        protected GravArrow currentShiftDir;

        public void PlayShiftAnimation(GravArrow dirToPlay)
        {
            //starting this function
            if(!bPlayShiftAnimation)
            {
                bPlayShiftAnimation = true;
                internalShiftTime = 0;
                anim.SetLayerWeight(1, 1);
                switch (dirToPlay)
                {
                    case GravArrow.backArrow:
                        anim.SetFloat(backForwarLeftRightHash, 1);
                        break;
                    case GravArrow.forwardArrow:
                        anim.SetFloat(backForwarLeftRightHash, 2);
                        break;
                    case GravArrow.leftArrow:
                        anim.SetFloat(backForwarLeftRightHash, 3);
                        break;
                    case GravArrow.rightArrow:
                        anim.SetFloat(backForwarLeftRightHash, 4);
                        break;
                }
                Debug.Log("DistanceJoint2D " + dirToPlay);
                return;
            }
            internalShiftTime += Time.deltaTime;
            if (internalShiftTime >= shiftAnimTime)
            {
                anim.SetLayerWeight(1, 0);
                anim.SetFloat(backForwarLeftRightHash, 0);
                bPlayShiftAnimation = false;
            }
        }

        protected void Update()
        {

            if (bPlayShiftAnimation)
                PlayShiftAnimation(currentShiftDir);

            if (!gravityShifter)
            { 
                gravityShifter = AManager.instance.gravityShifter;
                gravityShifter.playerAnimation = this;
            }

            walkSpeed = playerRB.velocity.sqrMagnitude;
            anim.SetFloat(walkHash, walkSpeed/velocityDamper);

            if (gravityShifter.bEnableGravityShift && !bShowGrav)
                ShowGrav(true);

            if (!gravityShifter.bEnableGravityShift && bShowGrav)
                ShowGrav(false);
        }

        

        protected void ShowGrav(bool bShow)
        {
            bShowGrav = bShow;
            foreach(GameObject go in unhideForGrav)
            {
                go.SetActive(bShow);
            }
            anim.SetBool(hasGravHash, bShow);
        }
    }

}