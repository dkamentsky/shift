﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPSCamera : MonoBehaviour
{
    public bool lockCursor = true;
    public KeyCode exitCursorLock = KeyCode.Escape;
    public KeyCode unlockCursorLock = KeyCode.Mouse0;
    public string xAxisName = "Mouse X"; //Name For X Input
    public string yAxisName = "Mouse Y"; //Namer For Y Input specified in Input tab
    public bool bConstrainPitch = true;
    public float cameraPitchLimit = 85f;
    public float cameraSensitivity = 2f;
    public bool bSmoothCamera = true;
    public float smoothAmount = 5f;
    public Transform m_camera;

    protected bool bIsCursorLocked;
    protected Vector2 lookInput;

    public void Start()
    {
        m_camera = Camera.main.transform;
    }


    public void PerformRotation(Transform m_character, float sensitivityMultiplier)
    {
        float sensitivity = cameraSensitivity * sensitivityMultiplier;
        lookInput = new Vector2(-Input.GetAxis(yAxisName) * sensitivity, Input.GetAxis(xAxisName) * sensitivity);

        //Add current angle to inputValue
        lookInput.x += m_camera.localEulerAngles.x;
        lookInput.y += m_character.localEulerAngles.y;

        //set target Rotations
        Quaternion charTargetRot = Quaternion.Euler(0f, lookInput.y, 0f);
        Quaternion cameraTargetRot = Quaternion.Euler(lookInput.x, m_camera.localEulerAngles.y, m_camera.localEulerAngles.z);

        //Clamp Pitch
        if (bConstrainPitch)
            cameraTargetRot = ClampRotationAroundXAxis(cameraTargetRot);

        //smooth Rotate
        if (bSmoothCamera)
        {
            m_camera.localRotation = Quaternion.Slerp(m_camera.localRotation, cameraTargetRot, smoothAmount * Time.deltaTime);
            m_character.localRotation = Quaternion.Slerp(m_character.localRotation, charTargetRot, smoothAmount * Time.deltaTime);
        }
        //janky Rotate
        else
        {
            m_camera.localRotation = cameraTargetRot;
            m_character.localRotation = charTargetRot;
        }
        UpdateCursorLock();
    }

    public void SetCursorLock(bool value)
    {
        lockCursor = value;

        if (!lockCursor)
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }

    public void UpdateCursorLock()
    {
        if (lockCursor)
            InternalLockUpdate();
    }

    private void InternalLockUpdate()
    {
        if (Input.GetKeyUp(exitCursorLock))
            bIsCursorLocked = false;
        else if (Input.GetKeyUp(unlockCursorLock))
            bIsCursorLocked = true;
        if (bIsCursorLocked)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
        else if (!bIsCursorLocked)
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }

    Quaternion ClampRotationAroundXAxis(Quaternion q)
    {
        q.x /= q.w;
        q.y /= q.w;
        q.z /= q.w;
        q.w = 1.0f;

        float angleX = 2.0f * Mathf.Rad2Deg * Mathf.Atan(q.x);

        angleX = Mathf.Clamp(angleX, -cameraPitchLimit, cameraPitchLimit);

        q.x = Mathf.Tan(0.5f * Mathf.Deg2Rad * angleX);

        return q;
    }
}
