﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Shift
{
    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(CapsuleCollider))]
    public class CharacterMotor : MonoBehaviour
    {
        //A struct that holds old the data relevant to groundCheck
        public struct GroundPoint
        {
            public Vector3 normal;
            public Vector3 groundpoint;
        }

        public float movementForce = 4f;[Tooltip("Force to apply on each input till we get to the speed cap")]
        public float maxSpeed = 20f;
        public float runMultiplier = 1.4f;[Tooltip("the cap will be multiplied this when running")]
        public float jumpForce = 3f;[Tooltip("force to apply to the player when jumping")]
        public float shellOffset = 0.01f;[Tooltip("reduce the radius by that ratio to avoid getting stuck in wall (a value of 0.1f is nice)")]
        public float climbThreshold = 0.01f;[Tooltip("The distance from the body the feet can move before applying upward force")]

        public GroundPoint currentGroundPoint; //the current point the character is grounded on
        public FootCollider footCol;[Tooltip("this is a reference to the characters foot colider")]
        [HideInInspector]
        public CapsuleCollider characterCollider;
        [HideInInspector]
        public Rigidbody rb;
        [HideInInspector]
        public bool bDisableMovement = false; // this will stop all update functions to allow direct manipulation of the motor
        //[HideInInspector]
        public bool bIsGrounded = false; //this will be true if the player is grounded
        [HideInInspector]
        public bool bGravityShift = false;
        [HideInInspector]
        public Transform playerFrame;

        protected float legHeight;
        public FPSCamera pcCam;
        public Transform groundPVisual;
        public Transform gravityLocaor;
        protected PlayerController pcController;




        protected void Start()
        {
            playerFrame = transform.parent;
            rb = gameObject.GetComponent<Rigidbody>();
            characterCollider = gameObject.GetComponent<CapsuleCollider>();
            legHeight = Vector3.Distance(transform.position, footCol.transform.position);
        }

        public void FixedUpdate()
        {
            if (!pcController)
                pcController = AManager.instance.playerController;

            GroundCheck();
            pcCam.PerformRotation(rb.transform, 1);
            if (pcController.jumpButton.bIsPressed)
                Jump();
            if (bIsGrounded)
                Climb();
            if (pcController.moveInput.sqrMagnitude > float.Epsilon && bIsGrounded)
                MoveCharachter(pcController.moveInput);
            if (bGravityShift)
                InternalRotateTowardsGravity();
        }

        public void LateUpdate()
        {

        }

        protected void GroundCheck()
        {
            RaycastHit hit;
            if (Physics.SphereCast(transform.position, footCol.col.radius * (1.0f - shellOffset), -transform.up, out hit, legHeight, Physics.DefaultRaycastLayers, QueryTriggerInteraction.Ignore))
            {
                bIsGrounded = true;
                currentGroundPoint.groundpoint = hit.point;
                currentGroundPoint.normal = hit.normal;
            }
            else
            {
                bIsGrounded = false;
                currentGroundPoint.normal = gameObject.transform.up;
            }
            groundPVisual.position = currentGroundPoint.groundpoint;
        }

        protected void Climb()
        {
            Vector3 climbVector;
            Vector3 feetPos = footCol.col.transform.position - footCol.transform.up * footCol.col.radius;

            climbVector = currentGroundPoint.groundpoint - feetPos;

            if(climbVector.sqrMagnitude > climbThreshold)
            {
                footCol.rb.MovePosition(footCol.rb.transform.position + climbVector);
            }
        }

        protected void Jump()
        {
            if (bIsGrounded)
            {
                rb.AddForce(rb.transform.up * jumpForce, ForceMode.VelocityChange);
            }
        }

        public void RotateTowardsGravity()
        {
            InternalRotateTowardsGravity();
        }

        protected void InternalRotateTowardsGravity()
        {
            transform.parent = playerFrame.parent;
            playerFrame.position = transform.position;
            transform.parent = playerFrame;
            playerFrame.rotation = gravityLocaor.rotation;
        }


        /// <summary>
        /// This functikon will add force to the rigid body to move it in the direction of
        /// the vector 3 input. If the Rigid body is in Air it won't apply z force. Input should be normalised
        /// speed will be applied using variables in the motor Class
        /// </summary>
        /// <param name="input">input to move the character verticle and horizontally and apply jump force</param>
        public void MoveCharachter(Vector2 input)
        {
            Vector3 desiredMoveDir = Vector3.Normalize(rb.transform.forward * input.y + rb.transform.right * input.x);
            desiredMoveDir *= movementForce;

            if (rb.velocity.sqrMagnitude < maxSpeed)
                rb.AddForce(desiredMoveDir, ForceMode.Impulse);
        }
    }
}