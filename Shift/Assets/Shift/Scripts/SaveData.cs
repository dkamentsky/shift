﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Shift
{
    public class SaveInfo
    {
        public int saveLevel;
        public int saveTriggerProgress;
        public Vector3 savePos;
        public Quaternion saveRot;
        public ShiftAxis saveGrav;

        public SaveInfo(int _saveLevel, int _saveTriggerProgress, Vector3 _savePos, Quaternion _saveRot, ShiftAxis _saveGrav)
        {
            saveLevel = _saveLevel;
            saveTriggerProgress = _saveTriggerProgress;
            savePos = _savePos;
            saveRot = _saveRot;
            saveGrav = _saveGrav;
        }
        public string PrintAllData()
        {
            string data = ("Saved Level #: " + saveLevel +
                           " Saved Trigger progress: " + (saveTriggerProgress + 1) +
                           " Position: (" + savePos.x + " , " + savePos.y + " , " + savePos.z + ") " +
                           " Rotation: (" + saveRot.eulerAngles.x + " , " + saveRot.eulerAngles.y + " , " + saveRot.eulerAngles.z + ") " +
                           " Gravity: " + saveGrav);
            return data;
        }
    }

    [System.Serializable]
    public class SaveData
    {
        public int saveLevel;
        public int saveTriggerProgress;
        public float[] savePos;
        public float[] saveRot;
        public ShiftAxis saveGrav;

        public SaveData(SaveInfo save)
        {
            saveLevel = save.saveLevel;
            saveTriggerProgress = save.saveTriggerProgress;
            savePos = new float[3];
            savePos[0] = save.savePos.x;
            savePos[1] = save.savePos.y;
            savePos[2] = save.savePos.z;
            saveRot = new float[4];
            saveRot[0] = save.saveRot.w;
            saveRot[1] = save.saveRot.x;
            saveRot[2] = save.saveRot.y;
            saveRot[3] = save.saveRot.z;
            saveGrav = save.saveGrav;
        }
        public SaveInfo GetSaveInfo()
        {
            Vector3 _SavePos = new Vector3(savePos[0], savePos[1], savePos[2]);
            Quaternion _SaveRot = new Quaternion(saveRot[0], saveRot[1], saveRot[2], saveRot[3]);

            SaveInfo data = new SaveInfo(saveLevel, saveTriggerProgress, _SavePos, _SaveRot, saveGrav);
            return data;
        }
        public string PrintAllData()
        {
            Quaternion playerRot = new Quaternion(saveRot[0], saveRot[1], saveRot[2], saveRot[3]);
            string data = ("Saved Level #: " + saveLevel +
                           "Saved Trigger progress: " + (saveTriggerProgress + 1) +
                           " Position: (" + savePos[0] + " , " + savePos[1] + " , " + savePos[2] + ") " +
                           " Rotation: (" + playerRot.eulerAngles.x + " , " + playerRot.eulerAngles.y + " , " + playerRot.eulerAngles.z + ") " +
                           " Gravity: " + saveGrav);
            return data;
        }
    }
}