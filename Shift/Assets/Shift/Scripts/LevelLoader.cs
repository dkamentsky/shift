﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Shift
{ 
    public class LevelLoader : MonoBehaviour
    {
        public int levelNumber = 0;
        [HideInInspector]
        public MainMenu mainMenu;

        public void SetLevelNumber(int levelToSet)
        {
            levelNumber = levelToSet;
        }
        public void StreamNextLevel()
        {
            levelNumber++;
            Debug.Log("scene loaded at " + levelNumber);
            SceneManager.LoadSceneAsync(levelNumber, LoadSceneMode.Additive);
        }
        public void UnloadLastScene()
        {
            int lastLevel = levelNumber - 1;
            Debug.Log("scene unloaded at " + lastLevel);
            SceneManager.UnloadSceneAsync(lastLevel);
            SceneManager.sceneLoaded += OnSceneLoaded;
        }
        void OnSceneLoaded(Scene scene, LoadSceneMode mode)
        {
            mainMenu.sceneProgressManager = FindObjectOfType<SceneProgressManager>();
            SceneManager.sceneLoaded -= OnSceneLoaded;
        }
    }

}