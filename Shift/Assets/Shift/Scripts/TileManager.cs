﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shift
{
    public class TileManager : MonoBehaviour
    {
        public bool bShouldTile = true;
        public bool bShouldTileX = true;
        public bool bShouldTileZ = true;
        public Vector2 tileSize;
        public List<ShiftTile> tilesToManage = new List<ShiftTile>();
        public int xSize = 3;
        public int zSize = 3;
        public ShiftTile[,] tileMatrix = new ShiftTile[3,3];
        public ShiftTile[,] originalTileMatrix = new ShiftTile[3,3];


        protected bool bIsXPos = false;
        protected bool bIsXNeg = false;
        protected bool bIsZPos = false;
        protected bool bIsZNeg = false;

        protected bool bMatrixFull = false;

        protected void Start()
        {
            FillTileMatrix();
            originalTileMatrix = tileMatrix;
        }

        protected void Update()
        {
            if (!bMatrixFull)
                FillTileMatrix();
            if (bMatrixFull && bShouldTile)
                if(CheckBorders())
                    MoveTiles();
        }
        public void ArangeTileHierarchy()
        {
            int _SiblingIndex = 0;
            for (int x = 0; x < xSize; x++)
            {
                for (int z = 0; z < zSize; z++)
                {
                    tileMatrix[x, z].transform.SetSiblingIndex(_SiblingIndex);
                    _SiblingIndex++;
                }
            }
        }
        public void SortTiles()
        {
            ShiftTile[] tempTiles = gameObject.GetComponentsInChildren<ShiftTile>();
            foreach (ShiftTile tile in tempTiles)
            {
                if (!tilesToManage.Contains(tile))
                    tilesToManage.Add(tile);
            }
            //finding the position of the lowest point for comparison
            float lowestX = tilesToManage[0].transform.position.x;
            float lowestZ = tilesToManage[0].transform.position.y;
            foreach(ShiftTile tile in tilesToManage)
            {
                if (lowestX > tile.transform.position.x)
                    lowestX = tile.transform.position.x;
                if (lowestZ > tile.transform.position.z)
                    lowestZ = tile.transform.position.z;
            }
            
            float _nextLowestX = lowestX;
            float _nextLowestZ = lowestZ;
            int tileX = 0;
            int tileZ = 0;
            bool bCheckX = false;
            bool bCheckZ = false;
            do
            {
                foreach (ShiftTile tile in tilesToManage)
                {
                    if (tile.transform.position.x == lowestX)
                        tile.xPos = tileX;
                    if (tile.transform.position.z == lowestZ)
                    { 
                        tile.zPos = tileZ;
                    }
                    if (_nextLowestX == lowestX && tile.transform.position.x > lowestX)
                        _nextLowestX = tile.transform.position.x;
                    if (_nextLowestZ == lowestZ && tile.transform.position.z > lowestZ)
                        _nextLowestZ = tile.transform.position.z;
                    if (tile.transform.position.x > lowestX && tile.transform.position.x < _nextLowestX)
                        _nextLowestX = tile.transform.position.x;
                    if (tile.transform.position.z > lowestZ && tile.transform.position.z < _nextLowestZ)
                        _nextLowestZ = tile.transform.position.z;
                }
                if (lowestX != _nextLowestX)
                {
                    lowestX = _nextLowestX;
                    tileX++;
                }
                else
                    bCheckX = true;

                if (lowestZ != _nextLowestZ)
                {
                    lowestZ = _nextLowestZ;
                    tileZ++;
                }
                else
                    bCheckZ = true;
            } while (!bCheckX | !bCheckZ);

            xSize = 1;
            zSize = 1;
            foreach(ShiftTile tile in tilesToManage)
            {
                if (tile.xPos > xSize - 1)
                    xSize = tile.xPos + 1;
                if (tile.zPos > zSize - 1)
                    zSize = tile.zPos + 1;
                tile.name = "tile(" + tile.xPos + "," + tile.zPos + ")";
            }
            FillTileMatrix();
            ArangeTileHierarchy();
        }

        protected void MoveTiles()
        {
            if(bIsXPos && bShouldTileZ)
            {
                for (int z = 0; z < zSize; z++)
                {
                    tileMatrix[0, z].transform.position = tileMatrix[xSize - 1, z].transform.position + new Vector3(tileSize.x, 0, 0);
                    for(int x = 0; x < xSize; x++)
                    {
                        tileMatrix[x,z].xPos -= 1;
                        if (tileMatrix[x, z].xPos < 0)
                            tileMatrix[x, z].xPos = xSize - 1;
                    }
                }
                FillTileMatrix();
                bIsXPos = false;
                return;
            }
            if(bIsXNeg && bShouldTileZ)
            {
                for (int z = 0; z < zSize; z++)
                {
                    tileMatrix[xSize - 1, z].transform.position = tileMatrix[0, z].transform.position - new Vector3(tileSize.x, 0, 0);
                    for (int x = 0; x < xSize; x++)
                    {
                        tileMatrix[x, z].xPos += 1;
                        if (tileMatrix[x, z].xPos > xSize - 1)
                            tileMatrix[x, z].xPos = 0;
                    }
                }
                FillTileMatrix();
                bIsXNeg = false;
                return; ;
            }
            if(bIsZPos && bShouldTileX)
            {
                for (int x = 0; x < xSize; x++)
                {
                    tileMatrix[x, 0].transform.position = tileMatrix[x, zSize - 1].transform.position + new Vector3(0, 0, tileSize.y);
                    for (int z = 0; z < zSize; z++)
                    {
                        tileMatrix[x, z].zPos -= 1;
                        if (tileMatrix[x, z].zPos < 0)
                            tileMatrix[x, z].zPos = zSize - 1;
                    }
                }
                FillTileMatrix();
                bIsZPos = false;
                return;
            }
            if(bIsZNeg && bShouldTileX)
            {
                for (int x = 0; x < xSize; x++)
                {
                    tileMatrix[x, zSize - 1].transform.position = tileMatrix[x, 0].transform.position - new Vector3(0, 0, tileSize.y);
                    for (int z = 0; z < zSize; z++)
                    {
                        tileMatrix[x, z].zPos += 1;
                        if (tileMatrix[x, z].zPos > zSize - 1)
                            tileMatrix[x, z].zPos = 0;
                    }
                }
                FillTileMatrix();
                bIsZNeg = false;
                return;
            }
        }

        protected void FillTileMatrix()
        {
            //return with an error message
            if(tilesToManage.Count > xSize * zSize)
                return;
            if (tilesToManage.Count < xSize * zSize)
                return;

            tileMatrix = new ShiftTile[xSize, zSize];
            foreach (ShiftTile tile in tilesToManage)
            { 
                tileMatrix[tile.xPos, tile.zPos] = tile;
            }

            bMatrixFull = true;
        }

        protected bool CheckBorders()
        {
            for (int i = 0; i <= xSize - 1; i++)
            {
                if (tileMatrix[i, 0].bPlayerIsIn)
                {
                    bIsZNeg = true;
                    return true;
                }
                if (tileMatrix[i, zSize - 1].bPlayerIsIn)
                {
                    bIsZPos = true;
                    return true;
                }
            }

            for (int i = 0; i < zSize - 1; i++)
            {
                if (tileMatrix[0, i].bPlayerIsIn)
                {
                    bIsXNeg = true;
                    return true;
                }
                if (tileMatrix[xSize - 1, i].bPlayerIsIn)
                {
                    bIsXPos = true;
                    return true;
                }
            }

            return false;
        }
    }
}
