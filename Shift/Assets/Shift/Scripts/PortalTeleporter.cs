﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shift
{
    [RequireComponent(typeof(ShiftPortal))]
    public class PortalTeleporter : MonoBehaviour
    {
        public ShiftPortal portal;

        public Transform otherPortal;

        public bool bShowPlayerDistance;

        [HideInInspector]
        public bool bIsPlayerInside = false;

        [HideInInspector]
        public bool bIsObjectInside = false;

        protected CharacterMotor player;
        protected List<Rigidbody> physObjs = new List<Rigidbody>();

        // check to see if the colider entering is the player
        protected void OnTriggerEnter(Collider other)
        {
            if (!player)
                player = other.GetComponent<CharacterMotor>();

            if (player)
                bIsPlayerInside = true;

            if (other.GetComponent<Rigidbody>() && !other.GetComponent<FootCollider>())
            {
                Rigidbody rb = other.GetComponent<Rigidbody>();
                if (!player)
                    physObjs.Add(rb);
                else if (rb != player.rb)
                    physObjs.Add(rb);
            }

            if (physObjs.Count > 0)
                bIsObjectInside = true;
        }

        protected void OnTriggerStay(Collider col)
        {
            if (bIsObjectInside)
            {
                foreach (Rigidbody rb in physObjs)
                {
                    //get the distance of the object to the portal;
                    float distance = transform.InverseTransformPoint(rb.transform.position).y;

                    if (distance <= float.Epsilon)
                    {
                        TeliportRigidBody(rb);
                    }
                }
            }
        }

        public void Update()
        {
            if (bIsPlayerInside)
            {
                //get the distance of the player to the portal;
                float distance = transform.InverseTransformPoint(player.transform.position).y;

                if (bShowPlayerDistance)
                    Debug.Log(distance);

                if (distance <= float.Epsilon)
                {
                    TeliportPlayer(player);
                }
            }
        }


        protected void TeliportPlayer(CharacterMotor player)
        {
            Vector3 vel = player.rb.velocity;
            float velSpeed = player.rb.velocity.magnitude;

            Transform playerFrame = player.playerFrame;

            Vector3 pos = portal.TeleportPosition(player.transform.position);

            //move the players frame to the position of the player so it can be teliported
            player.transform.parent = playerFrame.parent;
            playerFrame.position = player.transform.position;
            player.transform.parent = playerFrame;

            playerFrame.position = pos;

            playerFrame.rotation = portal.TeliportRotation(playerFrame.transform.rotation);

            portal.gravityShifter.PortalShift(portal);

            //vel = Vector3.Reflect(vel, transform.up);
            vel = transform.InverseTransformDirection(-vel);
            vel = otherPortal.TransformDirection(vel);
            vel = new Vector3(vel.x, -vel.y, vel.z);
            vel = Vector3.Normalize(vel) * velSpeed;
            player.rb.velocity = vel;
        }

        protected void TeliportRigidBody(Rigidbody rb)
        {
            Vector3 vel = rb.velocity;

            rb.Sleep();

            rb.position =  portal.TeleportPosition(rb.transform.position);

            rb.transform.rotation = portal.TeliportRotation(rb.transform.rotation);

            //vel = Vector3.Reflect(vel, transform.up);
            vel = transform.InverseTransformDirection(-vel);
            vel = otherPortal.TransformDirection(vel);
            rb.velocity = vel;
        }

        protected void OnTriggerExit(Collider other)
        {
            if (other.GetComponent<CharacterMotor>() && bIsPlayerInside)
            {
                bIsPlayerInside = false;
                player = null;
            }
            if (bIsObjectInside)
            {
                Rigidbody removeMe = null;
                foreach (Rigidbody rb in physObjs)
                {
                    if (rb.GetComponent<Collider>() == other)
                        removeMe = rb;
                }
                if (removeMe)
                    physObjs.Remove(removeMe);
                if (physObjs.Count <= 0)
                    bIsObjectInside = false;
            }
        }

    }
}