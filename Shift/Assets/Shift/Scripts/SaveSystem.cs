﻿using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Shift
{
    public static class SaveSystem
    {
        public static void SaveGameInfo(SaveInfo save)
        {
            Debug.Log(save.PrintAllData());
            BinaryFormatter formatter = new BinaryFormatter();
            string path = Application.persistentDataPath + "/ShiftGameInfo.ShiftData";
            FileStream stream = new FileStream(path, FileMode.Create);

            SaveData data = new SaveData(save);
            Debug.Log(data.PrintAllData());

            formatter.Serialize(stream, data);
            stream.Close();
        }

        public static SaveInfo LoadGameInfo()
        {
            string path = Application.persistentDataPath + "/ShiftGameInfo.ShiftData";
            if(File.Exists(path))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                FileStream stream = new FileStream(path, FileMode.Open);

                SaveData saveData = formatter.Deserialize(stream) as SaveData;
                Debug.Log(saveData.PrintAllData());
                stream.Close();
                SaveInfo info = saveData.GetSaveInfo();
                Debug.Log(info.PrintAllData());

                return info;
            }
            else
            {
                Debug.LogError("Save file not found in" + path);
                return null;
            }
        }
    }
}