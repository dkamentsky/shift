﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shift
{
    public class LOD_DisableEnable : MonoBehaviour
    {
        public int global_lod_Level = 0;
        public void ToggleLODs()
        {
            LODGroup[] lodToToggle = FindObjectsOfType<LODGroup>();
            foreach (LODGroup lod in lodToToggle)
                lod.ForceLOD(global_lod_Level);
        }
	}
}
