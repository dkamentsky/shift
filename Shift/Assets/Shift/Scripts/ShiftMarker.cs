﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shift
{
    [RequireComponent(typeof(SphereCollider))]
    public class ShiftMarker : MonoBehaviour
    {
        [HideInInspector]
        public bool bIsActive = false;
        public bool bUseLights = true;
        public bool bLightsOnWhenActive = false;
        public List<Light> lightsToActivate = new List<Light>();
        public bool bChangeColor = false;
        protected Color startColor;
        public Color colorToChangeToo;
        public bool bChangeIntensity;
        public float startIntensity = 0;
        public float endIntensity = 1;
        protected float collisionRadius;

        public bool bUseMaterialAffect = false;
        public string propertyToAffect = "_EmissionColor";
        public Color startingEmmisive;
        public Color endEmmissive;
        public bool bEmmisiveOnWhenActive = false;
        public Color inactiveEmmissive;
        protected MaterialPropertyBlock propBlock;
        protected Renderer _renderer;


        public void Awake()
        {
            if(bUseLights)
                startColor = lightsToActivate[0].color;
            collisionRadius = GetComponent<SphereCollider>().radius;
            StateChange(true);
            foreach(Light light in lightsToActivate)
                light.intensity = startIntensity;
            if(bUseMaterialAffect)
            { 
                propBlock = new MaterialPropertyBlock();
                _renderer = GetComponent<Renderer>();
                if(!bEmmisiveOnWhenActive)
                {
                    _renderer.GetPropertyBlock(propBlock);
                    propBlock.SetColor(propertyToAffect, endEmmissive);
                    _renderer.SetPropertyBlock(propBlock);
                }
            }
        }

        public void Update()
        {
            if (bIsActive)
                SetProperties();
        }

        protected void OnTriggerEnter(Collider other)
        {
            if (other.GetComponent<CharacterMotor>())
            { 
                bIsActive = true;
                StateChange(true);
            }
        }

        protected void OnTriggerExit(Collider other)
        {
            if (other.GetComponent<CharacterMotor>())
            { 
                bIsActive = false;
                StateChange(false);
            }
        }

        protected void StateChange(bool bSetToActive)
        {
            if(bLightsOnWhenActive)
                foreach (Light light in lightsToActivate)
                    light.gameObject.SetActive(bSetToActive);
            if (bEmmisiveOnWhenActive)
                propBlock.SetColor(propertyToAffect, inactiveEmmissive);
        }

        protected void SetProperties()
        {
            float distance = Vector3.Distance(transform.position, Camera.main.transform.position)/collisionRadius;
            if(bUseLights)
            { 
                foreach(Light light in lightsToActivate)
                {
                    if (bChangeColor)
                        light.color = Color.Lerp(colorToChangeToo, startColor, distance);
                    if (bChangeIntensity)
                        light.intensity = Mathf.Lerp(endIntensity , startIntensity, distance);
                }
            }
            if (bUseMaterialAffect)
            {
                _renderer.GetPropertyBlock(propBlock);
                propBlock.SetColor(propertyToAffect, Color.Lerp(startingEmmisive, endEmmissive, distance));
                _renderer.SetPropertyBlock(propBlock);
            }
        }
    }
}