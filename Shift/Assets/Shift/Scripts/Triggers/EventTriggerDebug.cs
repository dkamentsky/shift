﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shift
{
    public class EventTriggerDebug : ShiftTrigger
    {
        public KeyCode debugTriggerButton = KeyCode.P;

        protected override void Update()
        {
            base.Update();

            if (Input.GetKeyDown(debugTriggerButton))
                InternalCallEvent();
        }

    }
}