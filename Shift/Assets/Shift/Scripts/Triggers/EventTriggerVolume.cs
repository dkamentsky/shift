﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shift
{
    public class EventTriggerVolume : ShiftTrigger
    {
        public bool bCanOnlyBeTriggeredByPlayer = true;

        protected void OnTriggerEnter(Collider other)
        {
            if (other.GetComponent<Rigidbody>() && !bCanOnlyBeTriggeredByPlayer)
                InternalCallEvent();
            if (other.GetComponent<CharacterMotor>())
                InternalCallEvent();
        }
    }
}