﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Shift
{
    public class EventRelayTriggerVolume : RelayTrigger
    {
        public bool bStartDirection = false;[Tooltip("If true moving from Trigger A to B will go foward and vice versa")]
        public SmartCollider triggerA;
        public SmartCollider triggerB;
        public SmartCollider middleTrigger;

        protected bool bHasDirection = false;
        protected SmartCollider firstTrigger;
        protected SmartCollider secondTrigger;
        protected bool bIsInitialised = false;
        public bool bIsGoingForward = false;
        public bool bIsGoingBackward = false;
        public bool bIsMiddle = false;
        public bool waitTillNextTrigger = false;

        protected override void Start()
        {
            base.Start();
            if (bStartDirection)
            {
                firstTrigger = triggerA;
                secondTrigger = triggerB;
                bHasDirection = true;
            }
        }
        protected void Update()
        {
            //Determins the direction of the relay
            if(!bHasDirection && !bStartDirection)
            {
                if(triggerA.bIsTriggered)
                {
                    firstTrigger = triggerA;
                    secondTrigger = triggerB;
                    bHasDirection = true;
                }
                if(triggerB.bIsTriggered)
                {
                    firstTrigger = triggerB;
                    secondTrigger = triggerA;
                    bHasDirection = true;
                }
            }

            if (waitTillNextTrigger && !firstTrigger.bIsTriggered && !secondTrigger.bIsTriggered)
            {
                Debug.Log("its working?");
                waitTillNextTrigger = false;
            }
            if (bHasDirection && !waitTillNextTrigger &&  firstTrigger.bIsTriggered | secondTrigger.bIsTriggered | middleTrigger.bIsTriggered)
            {
                waitTillNextTrigger = true;
                if (middleTrigger.bIsTriggered)
                    bIsMiddle = true;
                if(firstTrigger.bIsTriggered && bIsMiddle && !bIsGoingBackward)
                {
                    RelayBackward();
                    bIsGoingBackward = true;
                    bIsGoingForward = false;
                    bIsMiddle = false;
                    return;
                }
                if(secondTrigger.bIsTriggered && bIsMiddle && !bIsGoingForward)
                {
                    RelayFoward();
                    bIsGoingForward = true;
                    bIsGoingBackward = false;
                    bIsMiddle = false;
                    return;
                }
                if (firstTrigger.bIsTriggered && bIsGoingForward && !bIsMiddle)
                    bIsGoingForward = false;
                if (secondTrigger.bIsTriggered && bIsGoingBackward && !bIsMiddle)
                    bIsGoingBackward = false;
            }
        }
        protected void ResetStates()
        {
            bIsMiddle = false;
            bIsGoingForward = false;
            bIsGoingBackward = false;
        }
    }
}