﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shift
{
    public class EventTriggerAndVisible: ShiftTrigger
    {
        public bool bMustBePlayer = true;
        public List<Renderer> visabilityTriggers;
        public bool bMustBeInVolumeWhileLooking = true;
        public bool bCheckIfVisible = false;

        protected bool bIsIn = false;
        protected bool bIsSeen = false;

        protected override void Update()
        {
            base.Update();
            if(bIsIn)
                if (AreObjectsVisable() == bCheckIfVisible)
                    InternalCallEvent();
        }

        protected void OnTriggerEnter(Collider other)
        {
            if (other.GetComponent<Rigidbody>() && !bMustBePlayer)
                bIsIn = true;
            if (other.GetComponent<CharacterMotor>())
                bIsIn = true;
        }

        protected void OnTriggerExit(Collider other)
        {
            if (other.GetComponent<Rigidbody>() && !bMustBePlayer && bIsIn && bMustBeInVolumeWhileLooking)
                bIsIn = false;
            if (other.GetComponent<CharacterMotor>() && bIsIn && bMustBeInVolumeWhileLooking)
                bIsIn = false;
        }
        protected bool AreObjectsVisable()
        {
            Plane[] viewPlane = GeometryUtility.CalculateFrustumPlanes(Camera.main);
            foreach (Renderer ren in visabilityTriggers)
            {
                if (GeometryUtility.TestPlanesAABB(viewPlane, ren.bounds))
                    return true;
            }

            if (bDebugThisClass)
                Debug.Log(name + " is Visable");

            return false;
        }
        
    }
}
