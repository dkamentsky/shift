﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shift
{
    public class MarkerPathTrigger : ShiftTrigger
    {
        public List<ShiftMarker> markerPath = new List<ShiftMarker>();
        public bool bIsDirectional = false;
        public ShiftMarker finalMarker;
        protected bool bIsOnPath;
        protected Dictionary<ShiftMarker, bool> pathDictionary = new Dictionary<ShiftMarker, bool>();
        public ShiftMarker triggeredMarker;

        protected void Awake()
        {
            foreach (ShiftMarker marker in markerPath)
                pathDictionary.Add(marker, false);
        }

        protected override void Update()
        {
            base.Update();
            if (CheckIfOnPath())
            {
                ChangeActiveStatus();
                bIsOnPath = true;
                if (CheckIfPathFinished())
                {
                    if (bIsDirectional)
                    {
                        if (ShiftMarker.ReferenceEquals(triggeredMarker, finalMarker))
                        { 
                            InternalCallEvent();
                            ResetDictionary();
                        }
                    }
                    else
                    { 
                        InternalCallEvent();
                        ResetDictionary();
                    }
                }
            }
            else if(bIsOnPath)
                ResetDictionary();
        }

        protected bool CheckIfPathFinished()
        {
            if (pathDictionary.ContainsValue(false))
                return false;
            return true;
        }

        protected void ChangeActiveStatus()
        {
            foreach(ShiftMarker marker in markerPath)
            {
                if(marker.bIsActive)
                {
                    pathDictionary.Remove(marker);
                    pathDictionary.Add(marker, true);
                    triggeredMarker = marker;
                }
            }
        }

        protected void ResetDictionary()
        {
            bIsOnPath = false;
            foreach (ShiftMarker marker in markerPath)
            {
                pathDictionary.Remove(marker);
                pathDictionary.Add(marker, false);
            }
        }

        protected bool CheckIfOnPath()
        {
            foreach (ShiftMarker marker in markerPath)
                if (marker.bIsActive)
                    return true;
            return false;
        }
    }
}
