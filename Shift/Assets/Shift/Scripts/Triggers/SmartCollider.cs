﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shift
{
    public class SmartCollider : MonoBehaviour
    {
        public bool bIsTriggered = false;
        public Collider lastIn;
        public List<Collider> areIn;
        public bool bOnlyPlayerTriggers = true;

        private void OnTriggerEnter(Collider other)
        {
            //lastIn = other;
            //areIn.Add(other);
            if(other.GetComponent<CharacterMotor>() && bOnlyPlayerTriggers)
                bIsTriggered = true;
            else if(!bOnlyPlayerTriggers)
                bIsTriggered = true;
        }
        private void OnTriggerExit(Collider other)
        {
            if (other.GetComponent<CharacterMotor>() && bOnlyPlayerTriggers)
            { 
                bIsTriggered = false;
                return;
            }
            //if (areIn.Count == 0)
            //{
            //    bIsTriggered = false;
            //    lastIn = null;
            //}
        }
    }
}