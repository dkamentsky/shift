﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shift
{


    public class ShiftEvent : MonoBehaviour
    {
        public bool bCallOnce = true;
        protected bool bHasBeenCalled = false;

        public bool bDebugThisClass = false;

        protected virtual void Update()
        {

        }

        public void EventCall()
        {
            if(bHasBeenCalled && bCallOnce)
            {
                if (bDebugThisClass)
                    Debug.Log("The event " + name + ": has already been called by trigger");
                return;
            }
            InternalEventCall();
            bHasBeenCalled = false;
        }

        protected virtual void InternalEventCall()
        {
            if (bDebugThisClass)
                Debug.Log(name + " has been called");
        }
    }

    public class ShiftTrigger : MonoBehaviour
    {
        public ShiftEvent shiftEvent;

        public bool bDebugThisClass = false;
        public bool bCallOnce = true;
        public SceneProgressManager sceneProgressManager;
        protected bool bHasBeenCalled = false;

        protected void Start()
        {
            if (!shiftEvent)
                Debug.Log(name + "has no event attatched");
        }

        protected virtual void Update()
        {
        }

        public void FireTrigger()
        {
            InternalCallEvent();
        }

        protected virtual void InternalCallEvent()
        {
            if (bDebugThisClass)
                Debug.Log(name + " has been triggered and is now calling " + name);
            //make sure if trigger only calls once it doesn't call twice
            if(bCallOnce)
            {
                if (bHasBeenCalled)
                    return;
                else
                    bHasBeenCalled = true;
            }
            sceneProgressManager.AdvanceTriggerProgress(this);
            shiftEvent.EventCall();
        }
    }
    public interface IRelayEvent
    {
        void RelayForward();
        void RelayBackward();
    }
    public class RelayTrigger : MonoBehaviour
    {
        public IRelayEvent relayEvent;

        virtual protected void Start()
        {
            relayEvent = GetComponentInParent<IRelayEvent>();
        }

        virtual protected void RelayFoward()
        {
            relayEvent.RelayForward();
        }

        virtual protected void RelayBackward()
        {
            relayEvent.RelayBackward();
        }
    }
}