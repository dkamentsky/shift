﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetGravityOnSceneStart : MonoBehaviour
{
    public Vector3 sceneGravity = new Vector3(0, -9.81f , 0);

	void Start ()
    {
        Physics.gravity = sceneGravity;
	}
}
