﻿using UnityEngine;
using System.Collections;


[ExecuteInEditMode] // Make mirror live-update even when not in play mode
public class RenderPortal : MonoBehaviour
{
    public Transform otherPortal;
    public int m_TextureSize = 256;
    public float m_ClipPlaneOffset = 0.07f;

    public LayerMask m_PortalLayers = -1;
    public float maxRenderDistance = 200f;

    private Hashtable m_PortalCameras = new Hashtable(); // Camera -> Camera table

    private RenderTexture m_PortalTexture = null;
    private int m_OldPortalTextureSize = 0;

    private static bool s_InsideRendering = false;

    // Helpers
    public static Quaternion QuaternionFromMatrix(Matrix4x4 m) { return Quaternion.LookRotation(m.GetColumn(2), m.GetColumn(1)); }
    public static Vector4 PosToV4(Vector3 v) { return new Vector4(v.x, v.y, v.z, 1.0f); }
    public static Vector3 ToV3(Vector4 v) { return new Vector3(v.x, v.y, v.z); }

    public static Vector3 ZeroV3 = new Vector3(0.0f, 0.0f, 0.0f);
    public static Vector3 OneV3 = new Vector3(1.0f, 1.0f, 1.0f);



    protected bool CheckRender(Renderer m_renderer)
    {
        Plane[] viewPlane = GeometryUtility.CalculateFrustumPlanes(Camera.current);
        if (GeometryUtility.TestPlanesAABB(viewPlane, m_renderer.bounds))
        {
            if (Vector3.Distance(Camera.current.transform.position, transform.position) <= maxRenderDistance)
            {
                return true;
            }
        }
        return false;
    }

    // This is called when it's known that the object will be rendered by some
    // camera. We render reflections and do other updates here.
    // Because the script executes in edit mode, reflections for the scene view
    // camera will just work!
    public void OnWillRenderObject()
    {
        Renderer renderer = GetComponent<Renderer>();

        if (!enabled || !renderer || !renderer.sharedMaterial || !renderer.enabled || !CheckRender(renderer))
            return;
        

        Camera cam = Camera.current;
        if (!cam)
            return;

        //Dont render render over camera
        if (cam.gameObject.layer == 8)
            return;


        // Safeguard from recursive calculations.        
        if (s_InsideRendering)
            return;
        s_InsideRendering = true;

        Camera portalCamera;
        CreateMirrorObjects(cam, out portalCamera);

        // find out the other portal's plane: position and normal in world space
        Vector3 pos = otherPortal.position;
        Vector3 normal = otherPortal.up;

        UpdateCameraModes(cam, portalCamera);

        Vector3 oldpos = cam.transform.position;
        Vector3 newpos;
        Quaternion newrot;

        GetPortalOffset(cam.transform,out newrot,out newpos);

        portalCamera.transform.position = newpos;
        portalCamera.transform.rotation = newrot;

        // Setup oblique projection matrix so that near plane is our portal
        // plane. This way we clip everything below/above it for free.
        Vector4 clipPlane = CameraSpacePlane(portalCamera, pos, normal, 1.0f);
        Matrix4x4 projection = portalCamera.projectionMatrix;
        CalculateObliqueMatrix(ref projection, clipPlane);
        portalCamera.projectionMatrix = projection;

        portalCamera.cullingMask = ~(1 << 4) & m_PortalLayers.value; // never render water layer
        portalCamera.targetTexture = m_PortalTexture;
        portalCamera.Render();
        portalCamera.transform.position = oldpos;
        Material[] materials = renderer.sharedMaterials;
        foreach (Material mat in materials)
        {
            if (mat.HasProperty("_ReflectionTex"))
                mat.SetTexture("_ReflectionTex", m_PortalTexture);
        }

        // Set matrix on the shader that transforms UVs from object space into screen
        // space. We want to just project portal texture on screen.
        Matrix4x4 scaleOffset = Matrix4x4.TRS(
            new Vector3(0.5f, 0.5f, 0.5f), Quaternion.identity, new Vector3(0.5f, 0.5f, 0.5f));
        Vector3 scale = transform.lossyScale;
        Matrix4x4 mtx = transform.localToWorldMatrix * Matrix4x4.Scale(new Vector3(1.0f / scale.x, 1.0f / scale.y, 1.0f / scale.z));
        mtx = scaleOffset * cam.projectionMatrix * cam.worldToCameraMatrix * mtx;
        foreach (Material mat in materials)
        {
            mat.SetMatrix("_ProjMatrix", mtx);
        }
        s_InsideRendering = false;
    }


    private void GetPortalOffset(Transform cam, out Quaternion rot, out Vector3 pos)
    {
        // Rotate Source 180 degrees so PortalCamera is mirror image of MainCamera
        Matrix4x4 destinationFlipRotation = Matrix4x4.TRS(ZeroV3, Quaternion.AngleAxis(180.0f, Vector3.forward), OneV3);
        Matrix4x4 sourceInvMat = destinationFlipRotation * transform.worldToLocalMatrix;

        // Calculate translation and rotation of MainCamera in Source 
        Quaternion cameraRotationInSourceSpace = QuaternionFromMatrix(sourceInvMat) * cam.transform.rotation;

        // Transform Portal Camera to World Space relative to Destination transform,
        // matching the Main Camera position/orientation
        rot = otherPortal.rotation * cameraRotationInSourceSpace;

        Vector3 m_pos;
        m_pos = transform.InverseTransformPoint(cam.position);
        pos = otherPortal.TransformPoint(new Vector3(-m_pos.x, -m_pos.y, m_pos.z));
    }

    // Cleanup all the objects we possibly have created
    void OnDisable()
    {
        if (m_PortalTexture)
        {
            DestroyImmediate(m_PortalTexture);
            m_PortalTexture = null;
        }
        foreach (DictionaryEntry kvp in m_PortalCameras)
            DestroyImmediate(((Camera)kvp.Value).gameObject);
        m_PortalCameras.Clear();
    }


    // Given position/normal of the plane, calculates plane in camera space.
    private Vector4 CameraSpacePlane(Camera cam, Vector3 pos, Vector3 normal, float sideSign)
    {
        Vector3 offsetPos = pos + normal * m_ClipPlaneOffset;
        Matrix4x4 m = cam.worldToCameraMatrix;
        Vector3 cpos = m.MultiplyPoint(offsetPos);
        Vector3 cnormal = m.MultiplyVector(normal).normalized * sideSign;
        return new Vector4(cnormal.x, cnormal.y, cnormal.z, -Vector3.Dot(cpos, cnormal));
    }

    // Adjusts the given projection matrix so that near plane is the given clipPlane
    // clipPlane is given in camera space. See article in Game Programming Gems 5 and
    // http://aras-p.info/texts/obliqueortho.html
    private static void CalculateObliqueMatrix(ref Matrix4x4 projection, Vector4 clipPlane)
    {
        Vector4 q = projection.inverse * new Vector4(
            sgn(clipPlane.x),
            sgn(clipPlane.y),
            1.0f,
            1.0f
        );
        Vector4 c = clipPlane * (2.0F / (Vector4.Dot(clipPlane, q)));
        // third row = clip plane - fourth row
        projection[2] = c.x - projection[3];
        projection[6] = c.y - projection[7];
        projection[10] = c.z - projection[11];
        projection[14] = c.w - projection[15];
    }

    // Extended sign: returns -1, 0 or 1 based on sign of a
    private static float sgn(float a)
    {
        if (a > 0.0f) return 1.0f;
        if (a < 0.0f) return -1.0f;
        return 0.0f;
    }

    // On-demand create any objects we need
    private void CreateMirrorObjects(Camera currentCamera, out Camera portalCamera)
    {
        portalCamera = null;

        // Reflection render texture
        if (!m_PortalTexture || m_OldPortalTextureSize != m_TextureSize)
        {
            if (m_PortalTexture)
                DestroyImmediate(m_PortalTexture);
            m_PortalTexture = new RenderTexture(m_TextureSize, m_TextureSize, 16);
            m_PortalTexture.name = "__MirrorReflection" + GetInstanceID();
            m_PortalTexture.isPowerOfTwo = true;
            m_PortalTexture.hideFlags = HideFlags.DontSave;
            m_OldPortalTextureSize = m_TextureSize;
        }

        // Camera for Portal
        portalCamera = m_PortalCameras[currentCamera] as Camera;
        if (!portalCamera) // catch both not-in-dictionary and in-dictionary-but-deleted-GO
        {
            GameObject go = new GameObject("Mirror Refl Camera id" + GetInstanceID() + " for " + currentCamera.GetInstanceID(), typeof(Camera), typeof(Skybox));
            portalCamera = go.GetComponent<Camera>();
            portalCamera.enabled = false;
            portalCamera.transform.position = otherPortal.transform.position;
            portalCamera.transform.rotation = otherPortal.transform.rotation;
            go.hideFlags = HideFlags.HideAndDontSave;
            m_PortalCameras[currentCamera] = portalCamera;
        }
    }

    //just coppies camera settings
    private void UpdateCameraModes(Camera src, Camera dest)
    {
        if (dest == null)
            return;
        // set camera to clear the same way as current camera
        dest.clearFlags = src.clearFlags;
        dest.backgroundColor = src.backgroundColor;
        if (src.clearFlags == CameraClearFlags.Skybox)
        {
            Skybox sky = src.GetComponent(typeof(Skybox)) as Skybox;
            Skybox mysky = dest.GetComponent(typeof(Skybox)) as Skybox;
            if (!sky || !sky.material)
            {
                mysky.enabled = false;
            }
            else
            {
                mysky.enabled = true;
                mysky.material = sky.material;
            }
        }
        // update other values to match current camera.
        // even if we are supplying custom camera&projection matrices,
        // some of values are used elsewhere (e.g. skybox uses far plane)
        dest.farClipPlane = src.farClipPlane;
        dest.nearClipPlane = src.nearClipPlane;
        dest.orthographic = src.orthographic;
        dest.fieldOfView = src.fieldOfView;
        dest.aspect = src.aspect;
        dest.orthographicSize = src.orthographicSize;
        // set the layer to 8 so it doesn't re render
        dest.gameObject.layer = 8;
    }

}
