﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shift
{
    public class SceneProgressManager : MonoBehaviour
    {
        public List<ShiftTrigger> triggerList = new List<ShiftTrigger>();
        public List<ShiftTrigger> triggerCacheList = new List<ShiftTrigger>();
        [HideInInspector]
        public int triggerProgress = -1;

        private void Start()
        {
            foreach (ShiftTrigger trigger in triggerList)
            {
                trigger.sceneProgressManager = this;
            }
        }
        public void AdvanceTriggerProgress(ShiftTrigger trigger)
        {
            if (triggerList.Contains(trigger))
                triggerProgress = triggerList.IndexOf(trigger);
        }
        public void SetTriggerProgress(int newTriggerProgress)
        {
            //return if we are already at that trigger progress
            if (newTriggerProgress <= triggerProgress)
                return;
            for (int i = 0; i <= newTriggerProgress; i++)
            {
                triggerList[i].FireTrigger();
            }
            triggerProgress = newTriggerProgress;
        }
        //ORDERING LIST GUI FUNCTIONS
        public void GetAllTriggers()
        {
            if (triggerList.Count > 0)
                triggerList.Clear();
            ShiftTrigger[] shiftTriggers = FindObjectsOfType<ShiftTrigger>();
            foreach(ShiftTrigger trigger in shiftTriggers)
            {
                triggerList.Add(trigger);
            }
        }
        public void SaveListOrder()
        {
            triggerCacheList.Clear();
            for (int i = 0; i < triggerList.Count; i++)
                triggerCacheList.Add(triggerList[i]);
        }
        public void LoadListOrder()
        {
            if (triggerCacheList.Count == 0)
                return;
            Dictionary<int, ShiftTrigger> triggerValues = new Dictionary<int, ShiftTrigger>();
            foreach (ShiftTrigger trigger in triggerList)
            {
                if(triggerCacheList.Contains(trigger))
                {
                    triggerValues.Add(triggerCacheList.IndexOf(trigger), trigger);
                }
            }
            for (int i = 0; i < triggerList.Count; i++)
            {
                if (triggerValues.ContainsKey(i))
                    triggerList[i] = triggerValues[i];
            }
        }
        public void MoveTriggerUp(ShiftTrigger trigger)
        {
            int triggerPos = triggerList.IndexOf(trigger);
            ShiftTrigger aboveTrigger = triggerList[triggerPos - 1];
            triggerList[triggerPos] = aboveTrigger;
            triggerList[triggerPos - 1] = trigger;
        }
        public void MoveTriggerDown(ShiftTrigger trigger)
        {
            int triggerPos = triggerList.IndexOf(trigger);
            ShiftTrigger aboveTrigger = triggerList[triggerPos + 1];
            triggerList[triggerPos] = aboveTrigger;
            triggerList[triggerPos + 1] = trigger;
        }
        public void SetTriggerNumber(int numberToSet, ShiftTrigger trigger)
        {
            if (triggerList.IndexOf(trigger) == numberToSet)
                return;
            int originalNumber = triggerList.IndexOf(trigger);
            ShiftTrigger triggerToSwap = triggerList[numberToSet];
            triggerList[numberToSet] = trigger;
            triggerList[originalNumber] = triggerToSwap;
        }
        //
    }
}
