﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shift
{
    public class MultiEvent : ShiftEvent
    {
        public List<ShiftEvent> eventList = new List<ShiftEvent>();

        protected override void InternalEventCall()
        {
            foreach (ShiftEvent shiftEvent in eventList)
                shiftEvent.EventCall();
        }
    }
}
