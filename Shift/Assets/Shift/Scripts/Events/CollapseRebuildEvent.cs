﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shift
{
    public class CollapseRebuildObject : MonoBehaviour
    {
        public bool bIsAsleep = false;
        public Vector3 restPosition;
        public Quaternion restRotation;
        public Rigidbody rb;
        public float velocityThreshold;
        public float timeToDespawn;
        protected bool bIsInitialised = false;


        public void LateUpdate()
        {
            if (rb.velocity.sqrMagnitude > float.Epsilon && !bIsInitialised)
                bIsInitialised = true;
            if(bIsInitialised && !bIsAsleep)
            {
                timeToDespawn -= Time.deltaTime;
                if (timeToDespawn <= 0)
                    RestObject();
                if (rb.velocity.sqrMagnitude < velocityThreshold)
                    RestObject();
            }

        }

        protected void RestObject()
        {
            rb.Sleep();
            rb.velocity = Vector3.zero;
            rb.isKinematic = true;
            restPosition = transform.position;
            restRotation = transform.rotation;
            bIsAsleep = true;
        }
    }
    public class CollapseRebuildEvent : ShiftEvent , IRelayEvent
    {
        
        public GameObject parentObjectsToMove;
        public GameObject parentFinalLocations;

        public bool bForceOnWake = true;
        public float wakeForce = 1.0f;
        public int assemblePhases = 4;
        public float velocityThreshold = .01f;
        public float timeToDespawn = 10f;

        public List<Transform> objectsToMove = new List<Transform>();
        public List<Transform> finalLocations = new List<Transform>();

        public List<GameObject> objectsToUnhide = new List<GameObject>();

        protected List<CollapseRebuildObject> rebuildObjects = new List<CollapseRebuildObject>();
        protected int currentPhase = 0;
        protected bool bSleepCheck = false;
        protected bool bObjectsAreUnhidden = false;
        



        public void PopulateList()
        {
            Transform[] m_ObjectsToMove = parentObjectsToMove.GetComponentsInChildren<Transform>();

            foreach (Transform t in m_ObjectsToMove)
                objectsToMove.Insert(t.GetSiblingIndex(), t);
            objectsToMove.Remove(parentObjectsToMove.transform);

            Transform[] m_FinalLocations = parentFinalLocations.GetComponentsInChildren<Transform>();

            foreach (Transform t in m_FinalLocations)
            {
                finalLocations.Insert(t.GetSiblingIndex(), t);
            }
            finalLocations.Remove(parentFinalLocations.transform);
        }

        public void RelayForward()
        {
            if (currentPhase <= assemblePhases)
            {
                currentPhase++;
                DoPhase();
                if (currentPhase == assemblePhases)
                    EndUnhideObjects(true);
            }
        }
        public void RelayBackward()
        {
            if (currentPhase > 0)
            {
                if (bObjectsAreUnhidden)
                    EndUnhideObjects(false);
                currentPhase--;
                DoPhase();
            }
        }
        protected void EndUnhideObjects(bool bHide)
        {
            foreach(GameObject go in objectsToUnhide)
                go.SetActive(bHide);
            bObjectsAreUnhidden = bHide;
        }

        protected void DoPhase()
        {
            float phasesCompleted = (float)currentPhase / (float)assemblePhases;
            for(int i = 0; i < rebuildObjects.Count; i++)
            {
                Transform go = objectsToMove[i];
                go.position = Vector3.Lerp(rebuildObjects[i].restPosition, finalLocations[i].position, phasesCompleted);
                go.rotation = Quaternion.Lerp(rebuildObjects[i].restRotation, finalLocations[i].rotation, phasesCompleted);
            }
        }

        protected void KickPhysics()
        {
            bSleepCheck = true;
            foreach(Transform m_transform in objectsToMove)
            {
                Rigidbody rb = m_transform.gameObject.AddComponent<Rigidbody>() as Rigidbody;
                CollapseRebuildObject m_collapseObject = m_transform.gameObject.AddComponent<CollapseRebuildObject>() as CollapseRebuildObject;
                m_collapseObject.rb = rb;
                m_collapseObject.velocityThreshold = velocityThreshold;
                m_collapseObject.timeToDespawn = timeToDespawn;
                rebuildObjects.Add(m_collapseObject);
                
                if (bForceOnWake)
                    AddWakeForce(rb);
            }
            StoreList();
        }

        protected void AddWakeForce(Rigidbody rb)
        {
            Vector3 forceDir = Vector3.Normalize(Camera.main.transform.position - rb.transform.position);
            rb.AddForce(forceDir * wakeForce, ForceMode.Impulse);
        }

        protected void StoreList()
        {
            if (objectsToMove.Count != finalLocations.Count)
            {
                Debug.Log("ERROR: the number of transforms in object to move must be the same as in final locations");
                return;
            }

            for (int i = 0; i < objectsToMove.Count; i++)
                rebuildObjects.Add(objectsToMove[i].GetComponent<CollapseRebuildObject>());
            if (bDebugThisClass)
                Debug.Log("list stored");
        }

        protected bool CheckIfAllRested()
        {
            foreach (CollapseRebuildObject m_collapseObject in rebuildObjects)
                if (!m_collapseObject.bIsAsleep)
                    return false;
            return true;
        }


        protected override void InternalEventCall()
        {
            base.InternalEventCall();
            KickPhysics();
        }
    }
}
