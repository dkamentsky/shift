﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Shift
{
    public class LoadUnloadNextLevelEvent : ShiftEvent
    {
        public bool bLoadNextLevel = false;//if false unload last level

        protected LevelLoader levelLoader;

        protected override void InternalEventCall()
        {
            levelLoader = AManager.instance.levelLoader;
            if (bLoadNextLevel)
                levelLoader.StreamNextLevel();
            else
                levelLoader.UnloadLastScene();
        }
    }
}
