﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shift
{
    public class TilingStartStopEvent : ShiftEvent
    {
        public TileManager tileManagerToChange;
        public bool bStartTile;
        public bool bStartTileX;
        public bool bStartTileZ;

        protected override void InternalEventCall()
        {
            base.InternalEventCall();
            tileManagerToChange.bShouldTile = bStartTile;
            tileManagerToChange.bShouldTileX = bStartTileX;
            tileManagerToChange.bShouldTileZ = bStartTileZ;
        }
    }
}
