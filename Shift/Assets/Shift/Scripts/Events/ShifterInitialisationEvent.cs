﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shift
{
    public class ShifterInitialisationEvent : ShiftEvent
    {
        public Renderer gravityShifterObject;
        public bool bLockOnStart;
        protected int numberOfPhases = 4;
        protected int phase = 0;
        protected bool bDoPhase;
        protected PlayerController playerController;

        protected override void Update()
        {
            base.Update();

            if (!playerController)
                playerController = AManager.instance.playerController;

            if (bLockOnStart)
                LockOnStart();

            if (phase > 0 && bDoPhase)
                DoPhase();
        }

        protected void LockOnStart()
        {
            playerController.reverseShiftKey.bIsActive = false;
            playerController.shiftKey.bIsActive = false;
            playerController.rightShiftKey.bIsActive = false;
            playerController.leftShiftKey.bIsActive = false;
            bLockOnStart = false;
        }

        protected void DoPhase()
        {
            switch(phase)
            {
                case 0:
                    break;
                case 1:
                    playerController.reverseShiftKey.bIsActive = true;
                    playerController.shiftKey.bIsActive = true;
                    gravityShifterObject.enabled = false;
                    Debug.Log("First phase call");
                    break;
                case 2:
                    playerController.reverseShiftKey.bIsActive = false;
                    playerController.shiftKey.bIsActive = false;
                    Debug.Log("second phase call");
                    break;

                case 3:
                    playerController.leftShiftKey.bIsActive = true;
                    playerController.rightShiftKey.bIsActive = true;
                    Debug.Log("third phase call");
                    break;

                case 4:
                    playerController.reverseShiftKey.bIsActive = true;
                    playerController.shiftKey.bIsActive = true;
                    Debug.Log("Fourth phase call");
                    break;

                default:
                    break;
            }

            bDoPhase = false;
        }


        protected override void InternalEventCall()
        {
            base.InternalEventCall();
            if (phase < numberOfPhases)
            { 
                phase++;
                bDoPhase = true;
            }
            else
                if (bDebugThisClass)
                    Debug.Log("All Phases complete");
        }
    }
}
