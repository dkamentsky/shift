﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shift
{
    public class SetActiveInactive : ShiftEvent
    {
        public List<GameObject> objectsToSetActive;
        public List<GameObject> objectsToSetInactive;
        public bool bStartInactive = true;

        protected void Start()
        {
            if (bStartInactive)
                foreach (GameObject go in objectsToSetActive)
                    go.SetActive(false);
        }

        protected override void InternalEventCall()
        {
            base.InternalEventCall();
            foreach (GameObject go in objectsToSetActive)
                go.SetActive(true);
            foreach (GameObject go in objectsToSetInactive)
                go.SetActive(false);
            if (bDebugThisClass)
                Debug.Log("All objects in " + name + " active states hav been modified");
        }
    }
}