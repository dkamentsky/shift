﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shift
{
    public class SetActiveEvent : ShiftEvent
    {
        public List<GameObject> objectsToChange;
        public bool bMakeActive;
        public bool bStartInOppositeState = true;

        protected void Start()
        {
            if (bStartInOppositeState)
                foreach (GameObject go in objectsToChange)
                    go.SetActive(!bMakeActive);
        }

        protected override void InternalEventCall()
        {
            base.InternalEventCall();
            foreach (GameObject go in objectsToChange)
                go.SetActive(bMakeActive);
            if (bDebugThisClass)
                Debug.Log("All objects in " + name + " active states hav been modified");
        }
    }
}
