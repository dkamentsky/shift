﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shift
{
    public class TileWeaveEvent : ShiftEvent
    {
        public int xOffset = 0;
        public int zOffset = 0;
        public TileManager oldTileManager;
        public TileManager newTileManager;
        public bool bChangeActiveStatus = true;
        public bool bStopTiling = false;
        public bool bStopTilingX = false;
        public bool bStopTilingZ = false;
        protected bool bWeaveTiles = false;

        protected override void Update()
        {
            base.Update();
            if (bWeaveTiles)
            {
                WeaveTiles();
                bWeaveTiles = false;
                if (bStopTiling)
                {
                    oldTileManager.bShouldTile = false;
                    newTileManager.bShouldTile = false;
                }
                if (bStopTilingX)
                {
                    oldTileManager.bShouldTileX = false;
                    newTileManager.bShouldTileX = false;
                }
                if (bStopTilingZ)
                {
                    oldTileManager.bShouldTileZ = false;
                    newTileManager.bShouldTileZ = false;
                }
            }
        }

        protected void WeaveTiles()
        {

            Vector3 tileMovePosition = oldTileManager.originalTileMatrix[0, 0].transform.position + 
                new Vector3(xOffset*newTileManager.tileSize.x, 0 , zOffset*newTileManager.tileSize.y);

            for (int z = 0; z < newTileManager.zSize; z++)
            {
                for (int x = 0; x < newTileManager.xSize; x++)
                {
                    //Debug.Log("( x: " + x + ", z: " + z + " ) position: " + tileMovePosition);
                    newTileManager.tileMatrix[x, z].transform.position = tileMovePosition;
                    tileMovePosition.x += newTileManager.tileSize.x;
                }
                tileMovePosition.x = newTileManager.tileMatrix[0, 0].transform.position.x;
                tileMovePosition.z += newTileManager.tileSize.y;
            }
        }
        protected override void InternalEventCall()
        {
            base.InternalEventCall();
            if (bChangeActiveStatus)
            {
                newTileManager.gameObject.SetActive(true);
            }
            bWeaveTiles = true;
        }
    }
}
