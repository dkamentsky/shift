﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shift
{ 
    public class GravityEvent : ShiftEvent
    {
        protected GravityShifter gravityShifter;
        protected ArrowMatControl arrowMatControl;

        public bool bEnableGravity = false;
        public bool bForwardShift = false;
        public bool bBackwardsShift = false;
        public bool bRightShift = false;
        public bool bLeftShift = false;

        protected override void Update()
        {
            base.Update();

            if (!gravityShifter)
                gravityShifter = AManager.instance.gravityShifter;
            if (!arrowMatControl)
                arrowMatControl = gravityShifter.arrowMatControl;
        }

        protected override void InternalEventCall()
        {
            base.InternalEventCall();

            gravityShifter.bEnableGravityShift = bEnableGravity;
            gravityShifter.bForwardShift = bForwardShift;
            gravityShifter.bBackwardsShift = bBackwardsShift;
            gravityShifter.bRightShift = bRightShift;
            gravityShifter.bLeftShift = bLeftShift;

            if (bEnableGravity)
                arrowMatControl.SetAllToDefault();
            arrowMatControl.ArrowActivate(GravArrow.forwardArrow, bForwardShift);
            arrowMatControl.ArrowActivate(GravArrow.backArrow, bBackwardsShift);
            arrowMatControl.ArrowActivate(GravArrow.leftArrow, bLeftShift);
            arrowMatControl.ArrowActivate(GravArrow.rightArrow, bRightShift);
        }
    }
}