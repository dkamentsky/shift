﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shift
{
    public class FollowPlayerEvent : ShiftEvent
    {
        public bool bShouldMove = false;
        public bool bShouldRotate = false;
        public bool bFollowPlayer = false;
        public bool bFreezeY = false;

        public Transform followTransform;

        protected Transform playerTransform;


        protected override void Update()
        {
            base.Update();

            if (!playerTransform && AManager.instance.gravityShifter)
                playerTransform = AManager.instance.gravityShifter.playerMotor.transform;

            if (bShouldMove && bFollowPlayer)
            {
                if (bFreezeY)
                    followTransform.position = new Vector3(playerTransform.position.x, followTransform.position.y, playerTransform.position.z);
                else
                    followTransform.position = playerTransform.position;
            }
            if (bShouldRotate && bFollowPlayer)
                followTransform.rotation = playerTransform.rotation;
        }

        protected override void InternalEventCall()
        {
            base.InternalEventCall();
            bFollowPlayer = !bFollowPlayer;
        }
    }
}
