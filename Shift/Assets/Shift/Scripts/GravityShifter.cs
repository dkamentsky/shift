﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shift
{

    /// <summary>
    /// The ShiftAxis Enum is to quickly indicate 6 axis that
    /// gravity can shift on and the player can look down
    /// </summary>
    public enum ShiftAxis
    {
        xP,
        xN,
        yP,
        yN,
        zP,
        zN,
    }

    /// <summary>
    /// Class that enables the player to shift the direction of gravity
    /// on a single axis P = positive N = neggative
    /// </summary>
    public class GravityShifter : MonoBehaviour
    {
        public bool bEnableGravityShift = false;[Tooltip("bool to disable gravity alltogether")]
        public int shiftCount = 5;[Tooltip("The amount of shifts left before you can't shift")]
        public bool bUseShiftCount = false;
        protected Camera playerCamera; // a reference to the main camera in the scene
        public Transform localOrthonormalBasis;[Tooltip("the parent of all the transforms in axis look at targets and the gravity locator and its pivot")]
        public Transform gravityLocator;[Tooltip("a transform indicating the direction of gravity as a vector relative to " +
                                                 "the cameras position attatching a mesh can help show the direction of gravity")]
        public Transform gravityPivot;[Tooltip("The parent transform of the gravity locator used to transform the direction of gravity")]
        public Transform locatorGroupReset;[Tooltip("Stores the rotation of the locator group relative to the direction of gravity")]
        public float gravityShiftDuration = 1f;[Tooltip("The time it takes to transition which axis gravity is working on")]
        public Vector3 defaultGravity = new Vector3(0f, -9.81f , 0f); //the vector representing the strenght and direction of gravity on start
        public CharacterMotor playerMotor;
        public ArrowMatControl arrowMatControl;
        public PlayerAnimation playerAnimation;


        /// <summary>
        /// toggle different shifts on or off
        /// </summary>
        public bool bForwardShift = false;
        public bool bBackwardsShift = false;
        public bool bRightShift = false;
        public bool bLeftShift = false;

        public Transform rotationTarget; // Rename this its to get the worlds rotation but link it to the player probably a better way of doing this but FUCK IT
        protected bool bIsShiftingGravity = false; //if this bool is true we are currently in the process of shifting gravity
        protected ShiftAxis currentLookAxis; //the axis the player is currently looking down assigned on checkLookAxis
        protected float internalGravityTime = 0f; //the current amount of time gravity has taken to transition
        protected bool bGravityDirReverse; //bool to tell if gravity direction should be reveresed
        protected bool bGravityRight; //bool to tell if gravity should go right
        protected bool bGravityLeft; //bool to tell if gravity should go left
        protected bool bShiftToWorldGravity = false; //bool to tell if we are switching to world gravity
        protected ShiftAxis saveAxis;
        protected bool bRunTwice;
        protected PlayerController playerController;
        protected ShiftPortal tempPortal;
        protected bool bDelayUpdateGravity = false;
        protected int delayUpdateGravityFrames = 2;
        protected int delayUpdateGravityCurrentFrame = 0;
        [HideInInspector]
        public Quaternion loadRot;
        [HideInInspector]
        public bool bIsLoadGame = false;


        //Attatch all debug logs to an if statement based on this bool to keep logspam down
        [SerializeField]
        private bool bDebugThisClass = false;

        protected void Start()
        {
            //get the main camera in the scene
            playerCamera = Camera.main;

            //set default gravity
            Physics.gravity = defaultGravity;

            //on start set the position of the gravity locator
            UpdateGravity(true);
            UpdateGravity(false);
            playerMotor.RotateTowardsGravity();

            gravityLocator.parent = gravityPivot.parent;
            locatorGroupReset.parent = gravityPivot.parent;
            gravityPivot.rotation = gravityPivot.parent.rotation;
            gravityLocator.parent = gravityPivot;
            locatorGroupReset.parent = gravityPivot;
        }

        protected void FixedUpdate()
        {
            //toggle to run UpdateGravity the frame after gravity has been rotated
            if(bDelayUpdateGravity)
            {
                delayUpdateGravityCurrentFrame++;
                if (delayUpdateGravityCurrentFrame >= delayUpdateGravityFrames)
                {
                    UpdateGravity(false);
                    playerMotor.RotateTowardsGravity();
                    bDelayUpdateGravity = false;

                    gravityLocator.parent = gravityPivot.parent;
                    locatorGroupReset.parent = gravityPivot.parent;
                    gravityPivot.rotation = gravityPivot.parent.rotation;
                    gravityLocator.parent = gravityPivot;
                    locatorGroupReset.parent = gravityPivot;

                    localOrthonormalBasis.rotation = locatorGroupReset.rotation;
                    if (bIsLoadGame)
                    {
                        playerMotor.transform.rotation = loadRot;
                        bIsLoadGame = false;
                    }
                    delayUpdateGravityCurrentFrame = 0;
                    Debug.Log(Physics.gravity);
                }
            }
            //if we haven't already get a reference too player controller
            if (!playerController)
                playerController = AManager.instance.playerController;

            //have the look at locator group and gravity pivot follow the position of the camera but not the rotation
            localOrthonormalBasis.position = playerCamera.transform.position;
            gravityPivot.position = playerCamera.transform.position;

            //if the player presses the key to shift gravity
            //if (Input.GetKeyDown(shiftKey) | Input.GetKeyDown(reverseShiftKey) | Input.GetKeyDown(leftShiftKey) | Input.GetKeyDown(rightShiftKey)
            //    && !bIsShiftingGravity && !bShiftToWorldGravity)
            if (playerController.shiftKey.bIsPressed | playerController.reverseShiftKey.bIsPressed | 
                playerController.leftShiftKey.bIsPressed | playerController.rightShiftKey.bIsPressed
                && !bIsShiftingGravity && !bShiftToWorldGravity)
            {
                //loop to end shift if it is disabled
                if (!bEnableGravityShift)
                {
                    if (bDebugThisClass)
                        Debug.Log("No gravity shift sorry");
                    return;
                }

                //loop to end shift if that direction is toggled off
                if (!bForwardShift && playerController.shiftKey.bIsPressed || !bBackwardsShift && playerController.reverseShiftKey.bIsPressed ||
                !bRightShift && playerController.rightShiftKey.bIsPressed || !bLeftShift && playerController.leftShiftKey.bIsPressed)
                {
                    if (bDebugThisClass)
                        Debug.Log("No gravity shift this direction sorry");
                    return;
                }

                //loop for limiting shifts
                if (bUseShiftCount)
                {
                    if (shiftCount > 0)
                    {
                        --shiftCount;
                        if (bDebugThisClass)
                            Debug.Log("shifts left = " + shiftCount);
                    }
                    else
                    {
                        if (bDebugThisClass)
                            Debug.Log("No more shifts left sorry");
                        return;
                    }
                }

                //input toggles for each direction
                if (playerController.reverseShiftKey.bIsPressed)
                    bGravityDirReverse = true;
                else
                    bGravityDirReverse = false;
                if (playerController.leftShiftKey.bIsPressed)
                    bGravityLeft = true;
                else
                    bGravityLeft = false;
                if (playerController.rightShiftKey.bIsPressed)
                    bGravityRight = true;
                else
                    bGravityRight = false;

                bIsShiftingGravity = true;
                CheckLookAxis();
            }
            //if we are shifting gravity continue to shift
            if (bIsShiftingGravity)
            {
                ShiftGravity();
                playerMotor.bGravityShift = true;
            }
            else if (playerMotor.bGravityShift && !bShiftToWorldGravity)
            {
                gravityLocator.parent = gravityPivot.parent;
                locatorGroupReset.parent = gravityPivot.parent;
                gravityPivot.rotation = gravityPivot.parent.rotation;
                gravityLocator.parent = gravityPivot;
                locatorGroupReset.parent = gravityPivot;
                playerMotor.bGravityShift = false;
            }
            //for gravity shifting volumes
            if (bShiftToWorldGravity)
            {
                ShiftToWorldGravity(saveAxis);
                playerMotor.bGravityShift = true;
            }
            else if (playerMotor.bGravityShift && !bIsShiftingGravity)
            {
                gravityLocator.parent = gravityPivot.parent;
                locatorGroupReset.parent = gravityPivot.parent;
                gravityPivot.rotation = gravityPivot.parent.rotation;
                gravityLocator.parent = gravityPivot;
                locatorGroupReset.parent = gravityPivot;
                playerMotor.bGravityShift = false;
            }

            if (bRunTwice)
                PortalShift(tempPortal);
        }


        /// <summary>
        /// function for getting world space from ShiftAxis enum
        /// </summary>
        /// <param name="inputAxis">axis to get transform from</param>
        /// <returns></returns>
        protected Vector3 GetWorldDirection(ShiftAxis inputAxis)
        {
            switch(inputAxis)
            {
                case ShiftAxis.xP:
                    return Vector3.right;
                case ShiftAxis.xN:
                    return -Vector3.right;
                case ShiftAxis.yP:
                    return Vector3.up;
                case ShiftAxis.yN:
                    return -Vector3.up;
                case ShiftAxis.zP:
                    return Vector3.forward;
                case ShiftAxis.zN:
                    return -Vector3.forward;
                default:
                    Debug.Log("invalid input");
                    return Vector3.zero;
            }
        }

        /// <summary>
        /// Call this function to shift gravity to a axis in world space.
        /// </summary>
        /// <param name="inputAxis">axis to shift too</param>
        public void ShiftToWorldGravity(ShiftAxis inputAxis)
        {
            if (!bShiftToWorldGravity)
            {
                bShiftToWorldGravity = true;
                saveAxis = inputAxis;
                if(bDebugThisClass)
                    Debug.Log("Shifting gravity on the " + inputAxis + " axis");
            }
            if (bIsShiftingGravity)
                return;
            //signal the start of gravity transition to the console
            if (bDebugThisClass && internalGravityTime == 0)
                Debug.Log("Start Gravity Transition" + Physics.gravity);
            //if the times specified in gravity shift duration hasn't elapsed add the time since the last frame
            if (internalGravityTime < gravityShiftDuration)
                internalGravityTime += Time.deltaTime;
            //if the time has elapsed set the time to gravity shift duration and set currently shifting to false
            if (internalGravityTime >= gravityShiftDuration)
            {
                internalGravityTime = gravityShiftDuration;
                bShiftToWorldGravity = false;
            }
            //rotate gravity locator towards current look axis and set the gravity vector to the gravity locators local position
            Quaternion desiredAxisRotation = Quaternion.FromToRotation(-localOrthonormalBasis.up, GetWorldDirection(inputAxis));
            gravityPivot.rotation = Quaternion.Slerp(gravityPivot.localRotation, desiredAxisRotation, internalGravityTime / gravityShiftDuration);
            UpdateGravity(false);
            //if we are done reset the time and the rotation of the locator group
            //either way log relevent information to the console
            if (!bShiftToWorldGravity)
            {
                internalGravityTime = 0;
                localOrthonormalBasis.rotation = locatorGroupReset.rotation;
                if (bDebugThisClass)
                    Debug.Log("Gravity has finished transitioning " + Physics.gravity);
            }
        }

        public void PortalShift(ShiftPortal portal)
        {
            gravityPivot.rotation = portal.TeliportRotation(gravityPivot.rotation);
            bDelayUpdateGravity = true;
            //if (!bRunTwice)
            //{
            //    tempPortal = portal;
            //    bRunTwice = true;
            //    return;
            //}


            //gravityPivot.rotation = portal.TeliportRotation(gravityPivot.rotation);
            //UpdateGravity(false);
            //localOrthonormalBasis.rotation = locatorGroupReset.rotation;

            //gravityLocator.parent = gravityPivot.parent;
            //locatorGroupReset.parent = gravityPivot.parent;
            //gravityPivot.rotation = gravityPivot.parent.rotation;
            //gravityLocator.parent = gravityPivot;
            //locatorGroupReset.parent = gravityPivot;

            //Debug.Log(Physics.gravity);

            //playerMotor.RotateTowardsGravity();
            //tempPortal = null;
            //bRunTwice = false;
        }
        
        public void ManualPhysicsShift(ShiftAxis inputAxis)
        {
            saveAxis = inputAxis;
            if (bDebugThisClass)
                Debug.Log("Shifting gravity on the " + inputAxis + " axis");
            
            gravityPivot.rotation = Quaternion.FromToRotation(-localOrthonormalBasis.up, GetWorldDirection(inputAxis));
            bDelayUpdateGravity =  true;
        }

        /// <summary>
        /// play the shift animation using this function
        /// </summary>
        protected void PlayShiftAnimation()
        {
            if (bGravityLeft)
                playerAnimation.PlayShiftAnimation(GravArrow.rightArrow);
            else if (bGravityRight)
                playerAnimation.PlayShiftAnimation(GravArrow.leftArrow);
            else if (bGravityDirReverse)
                playerAnimation.PlayShiftAnimation(GravArrow.forwardArrow);
            else
                playerAnimation.PlayShiftAnimation(GravArrow.backArrow);
        }

        /// <summary>
        /// Function to shift the gravity to the direction the player is currently looking over time
        /// </summary>
        public void ShiftGravity()
        {
            //signal the start of gravity transition to the console
            if (internalGravityTime == 0) 
            {
                if(bDebugThisClass)
                    Debug.Log("Start Gravity Transition" + Physics.gravity);
                PlayShiftAnimation();
            }
            //if the times specified in gravity shift duration hasn't elapsed add the time since the last frame
            if (internalGravityTime < gravityShiftDuration)
                internalGravityTime += Time.deltaTime;
            //if the time has elapsed set the time to gravity shift duration and set currently shifting to false
            if(internalGravityTime >= gravityShiftDuration)
            {
                internalGravityTime = gravityShiftDuration;
                bIsShiftingGravity = false;
            }
            //Store the orthonormal axis we are looking down
            Vector3 m_CurrentLookAxis = GetShiftAxis(currentLookAxis);
            //store the perpendicular orthonormal axis
            Vector3 m_PerpLookAxis = GetShiftAxisPerpendicular(currentLookAxis);
            //rotate gravity locator towards current look axis and set the gravity vector to the gravity locators local position
            Quaternion desiredAxisRotation = Quaternion.FromToRotation(m_CurrentLookAxis, -localOrthonormalBasis.up);
            //if you hit the gravity reverse button make gravity rotate in the opposite direction
            if(bGravityDirReverse)
                desiredAxisRotation = Quaternion.FromToRotation(m_CurrentLookAxis, localOrthonormalBasis.up);
            //if you hit the gravity right button make gravity rotate in the right direction
            if (bGravityRight)
                desiredAxisRotation = Quaternion.FromToRotation(m_PerpLookAxis, -localOrthonormalBasis.up);
            //if you hit the gravity left button make gravity rotate in the left direction
            if (bGravityLeft)
                desiredAxisRotation = Quaternion.FromToRotation(m_PerpLookAxis, localOrthonormalBasis.up);
            gravityPivot.rotation = Quaternion.Slerp(gravityPivot.localRotation, desiredAxisRotation, internalGravityTime / gravityShiftDuration);
            UpdateGravity(false);
            //if we are done reset the time and the rotation of the locator group
            //either way log relevent information to the console
            if(!bIsShiftingGravity)
            {
                internalGravityTime = 0;
                localOrthonormalBasis.rotation = locatorGroupReset.rotation;
                if (bDebugThisClass)
                    Debug.Log("Gravity has finished transitioning " + Physics.gravity);
            }
        }

        /// <summary>
        /// Takes the shift axis enum and runs it through a switch statement to find
        /// the corresponding orthonormalvector for the specified angle
        /// </summary>
        /// <param name="inputAxis">find transform for this axis</param>
        /// <returns>transform for the given axis</returns>
        public Vector3 GetShiftAxis(ShiftAxis inputAxis)
        {
            switch(inputAxis)
            {
                case ShiftAxis.xN:
                    return -localOrthonormalBasis.right;
                case ShiftAxis.xP:
                    return localOrthonormalBasis.right;
                case ShiftAxis.yN:
                    return -localOrthonormalBasis.up;
                case ShiftAxis.yP:
                    return localOrthonormalBasis.up;
                case ShiftAxis.zN:
                    return -localOrthonormalBasis.forward;
                case ShiftAxis.zP:
                    return localOrthonormalBasis.forward;

                default:
                    if (bDebugThisClass)
                        Debug.Log("Axis wasn't recognised returning down axis");
                    return -localOrthonormalBasis.up;
            }
        }
        /// <summary>
        /// Takes the shift axis enum and runs it through a switch statement to find
        /// the corresponding perpendicular orthonormalvector for the specified angle
        /// </summary>
        /// <param name="inputAxis">find transform for this axis</param>
        /// <returns>transform for the given axis</returns>
        public Vector3 GetShiftAxisPerpendicular(ShiftAxis inputAxis)
        {
            switch (inputAxis)
            {
                case ShiftAxis.xN:
                    return -localOrthonormalBasis.forward;
                case ShiftAxis.xP:
                    return localOrthonormalBasis.forward;
                case ShiftAxis.yN:
                    return -localOrthonormalBasis.up;
                case ShiftAxis.yP:
                    return localOrthonormalBasis.up;
                case ShiftAxis.zN:
                    return -localOrthonormalBasis.right;
                case ShiftAxis.zP:
                    return localOrthonormalBasis.right;

                default:
                    if (bDebugThisClass)
                        Debug.Log("Axis wasn't recognised returning down axis");
                    return -localOrthonormalBasis.up;
            }
        }

        /// <summary>
        /// this class serves to update the locator or the gravity vector to the vector not updating
        /// </summary>
        /// <param name="bUpdateLocator">a value if true will update the locator else gravity will update</param>
        protected void UpdateGravity(bool bUpdateLocator)
        {
            if (bUpdateLocator)
                gravityLocator.localPosition = Physics.gravity;
            else
            {
                //set locators parent to the locator group to get its local rotation within the group then parent it back
                //to the pivot
                rotationTarget.position = playerCamera.transform.position;
                gravityLocator.transform.parent = rotationTarget;
                Physics.gravity = gravityLocator.localPosition;
                gravityLocator.transform.parent = gravityPivot;
            }
        }

        /// <summary>
        /// This class gets the direction the player is looking at and sets it as currentLookAxis
        /// </summary>
        public void CheckLookAxis()
        {
            //this dictionary and the following statements are used to store the angle between the 
            //direction the player is looking against the 6 axis specified in the Shift Axis Enum
            Dictionary<float, ShiftAxis> valueAndAxis = new Dictionary<float, ShiftAxis>();

            valueAndAxis.Add(Vector3.Dot(playerCamera.transform.forward, -localOrthonormalBasis.right), ShiftAxis.xN);
            valueAndAxis.Add(Vector3.Dot(playerCamera.transform.forward, localOrthonormalBasis.right), ShiftAxis.xP);
            valueAndAxis.Add(Vector3.Dot(playerCamera.transform.forward, -localOrthonormalBasis.forward), ShiftAxis.zN);
            valueAndAxis.Add(Vector3.Dot(playerCamera.transform.forward, localOrthonormalBasis.forward), ShiftAxis.zP);

            //put all the values stored in the dictionary into a list and then sort it so the smallest
            //angle in the dictionary is at position 0
            List<float> keyList = new List<float>();
            keyList.AddRange(valueAndAxis.Keys);
            keyList.Sort();

            //Tell the console what axis we are looking down
            if(bDebugThisClass)
                Debug.Log("you are currently looking at the " + valueAndAxis[keyList[0]]);

            //set currwent look at axis
            currentLookAxis = valueAndAxis[keyList[0]];
        }

        public ShiftAxis FindGravityAxis()
        {
            //this dictionary and the following statements are used to store the angle between the 
            //direction of the orthonormal basis and world space.
            Dictionary<float, ShiftAxis> valueAndAxis = new Dictionary<float, ShiftAxis>();

            valueAndAxis.Add(Vector3.Dot(localOrthonormalBasis.up, -Vector3.up), ShiftAxis.yN);
            valueAndAxis.Add(Vector3.Dot(localOrthonormalBasis.up, Vector3.up), ShiftAxis.yP);
            valueAndAxis.Add(Vector3.Dot(localOrthonormalBasis.up, -Vector3.right), ShiftAxis.xN);
            valueAndAxis.Add(Vector3.Dot(localOrthonormalBasis.up, Vector3.right) + float.Epsilon, ShiftAxis.xP);
            valueAndAxis.Add(Vector3.Dot(localOrthonormalBasis.up, -Vector3.forward) + (2 * float.Epsilon), ShiftAxis.zN);
            valueAndAxis.Add(Vector3.Dot(localOrthonormalBasis.up, Vector3.forward) + (3 * float.Epsilon), ShiftAxis.zP);


            //put all the values stored in the dictionary into a list and then sort it so the smallest
            //angle in the dictionary is at position 0
            List<float> keyList = new List<float>();
            keyList.AddRange(valueAndAxis.Keys);
            keyList.Sort();

            //Tell the console what axis gravity is on
            if (bDebugThisClass)
                Debug.Log("Gravity is currently set to " + valueAndAxis[keyList[0]]);

            //Return the axis gravity is on
            return valueAndAxis[keyList[0]];
        }

    }
}