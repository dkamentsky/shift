﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shift
{
    public class DefaultLevelState : MonoBehaviour
    {
        public List<GameObject> toActivate = new List<GameObject>();
        public List<GameObject> toDeactivate = new List<GameObject>();

        // Use this for initialization
        void Start()
        {
            foreach (GameObject go in toActivate)
                go.SetActive(true);
            foreach (GameObject go in toDeactivate)
                go.SetActive(false);
        }

        public void SetAllActive()
        {
            foreach (GameObject go in toActivate)
                go.SetActive(true);
            foreach (GameObject go in toDeactivate)
                go.SetActive(true);
        }
        public void SetAllInActive()
        {
            foreach (GameObject go in toActivate)
                go.SetActive(false);
            foreach (GameObject go in toDeactivate)
                go.SetActive(false);
        }
        public void SetDefaultState()
        {
            foreach (GameObject go in toActivate)
                go.SetActive(true);
            foreach (GameObject go in toDeactivate)
                go.SetActive(false);
        }
    }
}