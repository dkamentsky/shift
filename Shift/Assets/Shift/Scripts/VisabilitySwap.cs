﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shift
{
    [RequireComponent(typeof(BoxCollider))]
    public class VisabilitySwap : MonoBehaviour
    {
        public List<GameObject> objectsToHide;
        public List<GameObject> objectsToShow;
        public List<Renderer> visabilityTriggers;
        public bool bHideShowOnStart = false;
        public bool bHideJustHideOnStart = true;

        protected bool bPlayerIn = false; //if true the player has crossed the threshold
        protected bool bNotLooking = false; //the aren't looking
        protected bool bHasSwitched = false;

        protected bool bDebugThisClass = false;

        public void Start()
        {
            if (bHideShowOnStart)
            {
                ShowList(false);
            }
            else if (bHideJustHideOnStart)
                JustHideList();
        }

        protected void Update()
        {
            if(bPlayerIn && !bHasSwitched)
            {
                if (AreObjectsVisable())
                {
                    ShowList(true);
                    bHasSwitched = true;
                }
            }
        }

        protected void OnTriggerEnter(Collider other)
        {
            if (other.GetComponent<CharacterMotor>())
            {
                if (bPlayerIn)
                    bPlayerIn = false;
                else
                    bPlayerIn = true;
            }
        }

        public void ShowList(bool show)
        {
            foreach (GameObject go in objectsToShow)
                go.SetActive(show);
            foreach (GameObject go in objectsToHide)
                go.SetActive(!show);
        }
        public void JustHideList()
        {
            foreach (GameObject go in objectsToShow)
                go.SetActive(false);
        }

        //returns true if you can see the objects returns false if you can't
        protected bool AreObjectsVisable()
        {
            Plane[] viewPlane = GeometryUtility.CalculateFrustumPlanes(Camera.main);
            foreach(Renderer ren in visabilityTriggers)
            {
                if (GeometryUtility.TestPlanesAABB(viewPlane, ren.bounds))
                    return false;
            }

            if (bDebugThisClass)
                Debug.Log("It's Invisible");

            return true;
        }
    }
}
