﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shift
{ 
    public class ShiftTile : MonoBehaviour
    {
        public int xPos;
        public int zPos;
        [HideInInspector]
        public int startXpos;
        public int startZpos;
        [HideInInspector]
        public bool bPlayerIsIn = false;
        public bool bDebugThisClass = false;

        protected void Awake()
        {
            startXpos = xPos;
            startZpos = zPos;
        }

        protected void OnTriggerEnter(Collider other)
        {
            if (!bPlayerIsIn)
            { 
                if (other.GetComponent<CharacterMotor>())
                    bPlayerIsIn = true;
                if(bDebugThisClass)
                    Debug.Log("x: " + xPos + " , y: " + zPos + " player is in this tile");
            }
        }

        protected void OnTriggerExit(Collider other)
        {
            if (bPlayerIsIn)
            {
                if (other.GetComponent<CharacterMotor>())
                    bPlayerIsIn = false;
                if (bDebugThisClass)
                    Debug.Log("x: " + xPos + " , y: " + zPos + " player is out of this tile");
            }
        }
    }
}
