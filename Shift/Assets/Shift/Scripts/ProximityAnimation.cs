﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shift
{
    [RequireComponent(typeof(SphereCollider))]
    public class ProximityAnimation : MonoBehaviour
    {
        public Animator anim;
        public string animName;
        public float minimumDistance;
        protected int animNameHash;
        protected bool bShouldAnimate = false;
        protected float colRadius;
        protected Transform playerTrans;
        protected int defaultStateHash;

        protected void Start()
        {
            animNameHash = Animator.StringToHash(animName);
            colRadius = gameObject.GetComponent<SphereCollider>().radius;
            defaultStateHash = anim.GetCurrentAnimatorStateInfo(0).fullPathHash;
        }

        protected void OnTriggerEnter(Collider other)
        {
            if (other.GetComponent<CharacterMotor>())
            {
                playerTrans = other.gameObject.transform;
                bShouldAnimate = true;
            }
        }
        protected void OnTriggerExit(Collider other)
        {
            if (other.GetComponent<CharacterMotor>())
            {
                bShouldAnimate = false;
                anim.Play(defaultStateHash, 0);
            }
        }

        protected void Update()
        {
            if (bShouldAnimate)
                AnimateByDistance();
        }

        public void AnimateByDistance()
        {
            float transitionAmount = (Vector3.Distance(playerTrans.position , gameObject.transform.position) - minimumDistance) / (colRadius - minimumDistance);
            if (transitionAmount < 0)
                transitionAmount = 0;
            if (transitionAmount > 1)
                transitionAmount = 1;
            transitionAmount = 1 - transitionAmount;
            anim.Play(animNameHash, 0, transitionAmount);
            anim.Update(0f);
        }
	}
}
