﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Shift
{
    public class MainMenu : MonoBehaviour
    {
        public List<GameObject> mainMenu = new List<GameObject>();
        public KeyCode mainMenuKey = KeyCode.Escape;
        public int levelToStart = 1;
        public string mainMenuSceneName = "MainMenu";
        public GameObject shiftGame;
        protected Scene activeScene;
        protected bool bMainMenuActive = false;
        public bool bSpawnNewShiftGame = true;
        public GameObject shiftGamePrefab;
        public Transform shiftSpawnPoint;
        protected bool bLevelIsLoaded = false;
        protected bool bIsNewGame = false;
        protected SaveInfo tempSaveInfo;
        public SceneProgressManager sceneProgressManager;


        protected void Update()
        {
            if (Input.GetKeyDown(mainMenuKey))
            {
                if (bMainMenuActive)
                    ResumeActiveScene();
                else
                    UnHideMainMenu();
            }
            if (Input.GetKeyDown(KeyCode.F))
                Debug.Log(AManager.instance.gravityShifter.FindGravityAxis().ToString());
        }
        protected void LateUpdate()
        {
            if (bLevelIsLoaded)
                InitializeGame();
        }
        

        public void SaveGame()
        {
            ResumeActiveScene();
            Transform player = AManager.instance.gravityShifter.playerMotor.transform;
            Quaternion gravRot = AManager.instance.gravityShifter.gravityPivot.rotation;
            //get the save data
            SaveInfo data = new SaveInfo(AManager.instance.levelLoader.levelNumber, sceneProgressManager.triggerProgress, player.position, player.rotation, 
                                         AManager.instance.gravityShifter.FindGravityAxis());
            SaveSystem.SaveGameInfo(data);
            UnHideMainMenu();
        }
        public void LoadGame()
        {
            tempSaveInfo = SaveSystem.LoadGameInfo();
            ResumeActiveScene();
            UnloadScenes();
            //if we are spawning a new game prefab then delete the current one and instantiate a new one on spawn
            if (bSpawnNewShiftGame)
            {
                if (shiftGame)
                    Destroy(shiftGame);
                shiftGame = Instantiate(shiftGamePrefab);
                shiftGame.SetActive(false);
            }

            activeScene = SceneManager.GetSceneByBuildIndex(0);
            SceneManager.LoadSceneAsync(tempSaveInfo.saveLevel, LoadSceneMode.Additive);
            activeScene = SceneManager.GetSceneByBuildIndex(tempSaveInfo.saveLevel);
            SceneManager.SetActiveScene(activeScene);
            SceneManager.sceneLoaded += OnSceneLoaded;
        }
        public void NewGame()
        {
            bIsNewGame = true;
            ResumeActiveScene();
            UnloadScenes();
            //if we are spawning a new game prefab then delete the current one and instantiate a new one on spawn
            if (bSpawnNewShiftGame)
            {
                if (shiftGame)
                    Destroy(shiftGame);

                shiftGame = Instantiate(shiftGamePrefab);
                shiftGame.SetActive(false);
            }

            activeScene = SceneManager.GetSceneByBuildIndex(0);
            SceneManager.LoadSceneAsync(levelToStart, LoadSceneMode.Additive);
            activeScene = SceneManager.GetSceneByBuildIndex(levelToStart);
            SceneManager.SetActiveScene(activeScene);
            SceneManager.sceneLoaded += OnSceneLoaded;
        }
        void OnSceneLoaded(Scene scene, LoadSceneMode mode)
        {
            shiftGame.SetActive(true);
            bLevelIsLoaded = true;
            sceneProgressManager = FindObjectOfType<SceneProgressManager>();
            SceneManager.sceneLoaded -= OnSceneLoaded;
        }
        protected void InitializeGame()
        {
            if (bIsNewGame)
            {
                AManager.instance.levelLoader.SetLevelNumber(levelToStart);
                AManager.instance.gravityShifter.playerMotor.transform.position = shiftSpawnPoint.position;
                AManager.instance.gravityShifter.playerMotor.transform.rotation = shiftSpawnPoint.rotation;
                bIsNewGame = false;
            }
            else
            {
                AManager.instance.levelLoader.SetLevelNumber(tempSaveInfo.saveLevel);
                AManager.instance.gravityShifter.playerMotor.transform.position = tempSaveInfo.savePos;
                AManager.instance.gravityShifter.loadRot = tempSaveInfo.saveRot;
                AManager.instance.gravityShifter.bIsLoadGame = true;
                AManager.instance.gravityShifter.ManualPhysicsShift(tempSaveInfo.saveGrav);
                //AManager.instance.gravityShifter.playerMotor.transform.rotation = tempSaveInfo.saveRot;
                sceneProgressManager.SetTriggerProgress(tempSaveInfo.saveTriggerProgress);
            }
            AManager.instance.levelLoader.mainMenu = this;
            bLevelIsLoaded = false;
        }

        public void UnloadScenes()
        {
            int c = SceneManager.sceneCount;
            for (int i = 0; i < c; i++)
            {
                Scene scene = SceneManager.GetSceneAt(i);
                if (scene.buildIndex != 0)
                {
                    Debug.Log(scene.name + " is being unloaded");
                    SceneManager.UnloadSceneAsync(scene);
                }
            }
        }

        public void ResumeGame()
        {
            ResumeActiveScene();
        }

        public void QuitGame()
        {
            Application.Quit();
        }

        protected void UnHideMainMenu()
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            foreach (GameObject go in mainMenu)
                go.SetActive(true);
            Time.timeScale = 0;
            bMainMenuActive = true;
        }

        protected void ResumeActiveScene()
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            foreach (GameObject go in mainMenu)
                go.SetActive(false);
            Time.timeScale = 1;
            bMainMenuActive = false;
        }
    }
}