﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shift
{
    public class ShiftPortal : MonoBehaviour
    {
        public Transform otherPortal;

        [HideInInspector]
        public GravityShifter gravityShifter;

        // Helpers
        public static Quaternion QuaternionFromMatrix(Matrix4x4 m) { return Quaternion.LookRotation(m.GetColumn(2), m.GetColumn(1)); }
        public static Vector4 PosToV4(Vector3 v) { return new Vector4(v.x, v.y, v.z, 1.0f); }
        public static Vector3 ToV3(Vector4 v) { return new Vector3(v.x, v.y, v.z); }

        public static Vector3 ZeroV3 = new Vector3(0.0f, 0.0f, 0.0f);
        public static Vector3 OneV3 = new Vector3(1.0f, 1.0f, 1.0f);

        public void Update()
        {
            if (gravityShifter == null)
                gravityShifter = AManager.instance.gravityShifter;
        }

        public Vector3 TeleportPosition(Vector3 pos)
        {
            Vector3 m_pos;
            m_pos = transform.InverseTransformPoint(pos);
            m_pos = otherPortal.TransformPoint(new Vector3(-m_pos.x, float.Epsilon, m_pos.z));
            return m_pos;
        }

        public Quaternion TeliportRotation(Quaternion rot)
        {
            // Rotate Source 180 degrees so PortalCamera is mirror image of MainCamera
            Matrix4x4 destinationFlipRotation = Matrix4x4.TRS(ZeroV3, Quaternion.AngleAxis(180.0f, Vector3.forward), OneV3);
            Matrix4x4 sourceInvMat = destinationFlipRotation * transform.worldToLocalMatrix;

            // Calculate translation and rotation of MainCamera in Source 
            Quaternion cameraRotationInSourceSpace = QuaternionFromMatrix(sourceInvMat) * rot;

            // Transform Portal Camera to World Space relative to Destination transform,
            // matching the Main Camera position/orientation
            return otherPortal.rotation * cameraRotationInSourceSpace;
        }
    }
}