﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shift
{
    public class Portal : MonoBehaviour
    {
        public Portal otherPortal;

        //[HideInInspector]
        public Camera portalCam; // a reference to the camera attatched to this portal
        protected Material portalMat;
        protected Camera playerCam;


        // Use this for initialization
        void Start()
        {
            playerCam = Camera.main;
            portalCam = GetComponentInChildren<Camera>();
            portalMat = GetComponentInChildren<Renderer>().material;

            if (otherPortal.portalCam.targetTexture != null)
            {
                otherPortal.portalCam.targetTexture.Release();
            }

        }

        void Update()
        {
            if(otherPortal.portalCam.targetTexture == null)
            {
                otherPortal.portalCam.targetTexture = new RenderTexture(Screen.width, Screen.height, 24);
                portalMat.mainTexture = otherPortal.portalCam.targetTexture;
            }
            PortalHit();
        }

        /// <summary>
        /// Feed this function a raycast hit when the player is looking at the portal
        /// </summary>
        /// <param name="hitLocation">location of the raytrace hit</param>
        /// <param name="hitNormal">normal of the ratrace hit</param>
        /// Vector3 hitLocation, Vector3 hitNormal
        public void PortalHit()
        {

            ////get the offset between the two portals
            //Vector3 offsetPosition = otherPortal.transform.position - transform.position;
            //otherPortal.portalCam.transform.position = hitLocation + offsetPosition;

            //set the portals local rotation to match the normal of the raycast hit
            //Quaternion targetRotation = Quaternion.LookRotation(playerCam.transform.forward, hitNormal);
            Quaternion targetRotation = playerCam.transform.rotation;
            otherPortal.portalCam.transform.rotation = targetRotation;
        }
    }
}
