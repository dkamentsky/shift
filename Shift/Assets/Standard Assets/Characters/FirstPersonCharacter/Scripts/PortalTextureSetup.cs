﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shift
{
    public class PortalTextureSetup : MonoBehaviour
    {
        public Camera cameraB;
        public Material cameraMatB;


        // Use this for initialization
        void Start()
        {
            if(cameraB.targetTexture != null)
            {
                cameraB.targetTexture.Release();
            }
            cameraB.targetTexture = new RenderTexture(Screen.width, Screen.height, 24);
            cameraMatB.mainTexture = cameraB.targetTexture;
        }
    }
    public class Portal2 : MonoBehaviour
    {
        public Transform otherPortal;
        protected Transform playerCamera;
        protected Transform portal;

        void Start()
        {
            portal = transform.parent;
            playerCamera = Camera.main.transform;
        }


        void Update()
        {
            Vector3 playerOffsetFromPortal = playerCamera.position - otherPortal.position;
            transform.position = portal.position + playerOffsetFromPortal;

            float angularDifference = Quaternion.Angle(portal.rotation, otherPortal.rotation); //angular difference between the two portals

            Quaternion portalRotationalDifference = Quaternion.AngleAxis(angularDifference, Vector3.up);
            Vector3 newCameraDirection = portalRotationalDifference * playerCamera.forward;
            transform.rotation = Quaternion.LookRotation(newCameraDirection, Vector3.up);
        }
    }
}
