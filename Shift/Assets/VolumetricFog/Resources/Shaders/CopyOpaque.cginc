	#include "UnityCG.cginc"
	#include "VolumetricFogOptions.cginc"

    struct appdata {
    	float4 vertex : POSITION;
		float2 texcoord : TEXCOORD0;
    };

	struct v2f {
	    float4 pos : SV_POSITION;
	    float2 uv: TEXCOORD0;
		float2 depthUV: TEXCOORD1;
		float2 depthUVNonStereo : TEXCOORD2;
	};

	sampler2D_float _CameraDepthTexture;
	sampler2D_float _VolumetricFogDepthTexture;
	sampler2D _MainTex;
	float4 _MainTex_ST;
	float4 _MainTex_TexelSize;
	sampler2D _VolumetricFog_OpaqueFrame;
	float _BlendPower;

	v2f vert(appdata v) {
    	v2f o;
    	o.pos = UnityObjectToClipPos(v.vertex);
    	o.uv = UnityStereoScreenSpaceUVAdjust(v.texcoord, _MainTex_ST);
   		o.depthUV = o.uv;
		o.depthUVNonStereo = v.texcoord;

    	#if UNITY_UV_STARTS_AT_TOP
    	if (_MainTex_TexelSize.y < 0) {
	        // Depth texture is inverted WRT the main texture
    	    o.depthUV.y = 1.0 - o.depthUV.y;
			o.depthUVNonStereo.y = 1.0 - o.depthUVNonStereo.y;
    	}
    	#endif

		return o;
	}	

	inline float getDepth(v2f i) {
		#if defined(FOG_ORTHO)
			float depth01 = UNITY_SAMPLE_DEPTH(tex2D(_CameraDepthTexture, i.depthUV));
			#if UNITY_REVERSED_Z
				depth01 = 1.0 - depth01;
			#endif
		#else
			float depth01 = Linear01Depth(UNITY_SAMPLE_DEPTH(tex2D(_CameraDepthTexture, i.depthUV)));
		#endif
		return depth01;
	}

	
	float4 frag (v2f i): SV_Target {

		float4 opaqueFrame = tex2D(_VolumetricFog_OpaqueFrame, i.uv);
		float4 transpFrame = tex2D(_MainTex, i.uv);

		#if defined(FOG_COMPUTE_DEPTH)
			float depthOpaque = getDepth(i);
			float depthTex = Linear01Depth(UNITY_SAMPLE_DEPTH(tex2D(_VolumetricFogDepthTexture, i.depthUVNonStereo)));
			if (depthTex < depthOpaque) {
				return lerp(transpFrame, opaqueFrame, opaqueFrame.a);
			}
		#endif

		float  t = saturate(_BlendPower + (1.0 - opaqueFrame.a));
		return lerp(opaqueFrame, transpFrame, t);
	}	
	
	