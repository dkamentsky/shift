﻿// Uncomment to render in orthographic mode
//#define FOG_ORTHO

// Uncomment to render the fog alone
//#define FOG_DEBUG

// Uncomment to enable screen-space mask based on mesh volumes
//#define FOG_MASK

#define FOG_MAX_POINT_LIGHTS 6
